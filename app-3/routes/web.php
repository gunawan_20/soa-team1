<?php

use App\Http\Controllers\MahasiswaController;
use Illuminate\Support\Facades\Route;
use App\http\Controllers\JadwalController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UniversitasController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\MatakuliahController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//universitas
Route::get('/',[UniversitasController::class,'index'])->name('Dashboard');

//mahasiswa
Route::get('/loginmahasiswa',[LoginController::class,'loginmahasiswa'])->name('login');
Route::post('/loginmahasiswasimpan',[LoginController::class,'loginmahasiswasimpan'])->name('loginmahasiswasimpan');
Route::get('/registermahasiswa',[LoginController::class,'registermahasiswa'])->name('register');
Route::post('/registermahasiswasimpan',[LoginController::class,'registermahasiswasimpan'])->name('registermahasiswa');
Route::get('/logoutmahasiswa',[LoginController::class,'logoutmahasiswa'])->name('logoutmahasiswa');

Route::get('/mahasiswa',[MahasiswaController::class,'datamahasiswa'])->name('mahasiswa')->middleware('auth');
Route::get('/tambahmahasiswa',[MahasiswaController::class,'tambahmahasiswa'])->name('tambah')->middleware('auth');
Route::post('/insertdatamahasiswa',[MahasiswaController::class,'insertdatamahasiswa'])->name('insertdatamahasiswa')->middleware('auth');

Route::get('/tampilkandatamahasiswa/{id}',[MahasiswaController::class,'tampilkandata'])->name('tampilkandata')->middleware('auth');
Route::post('/updatedatamahasiswa/{id}',[MahasiswaController::class,'updatedata'])->name('updatedata')->middleware('auth');

Route::get('/deletedatamahasiswa/{id}',[MahasiswaController::class,'delete'])->name('delete')->middleware('auth');

//new Matakuliah
Route::get('/matakuliah', [MatakuliahController::class, 'index'])->name('matakuliah');
Route::get('/matakuliah_user', [MatakuliahController::class, 'matakuliah_user'])->name('matakuliah_user');
Route::get('/tambahdata', [MatakuliahController::class, 'tambahdata'])->name('tambahdata');
Route::post('/insertdata', [MatakuliahController::class, 'insertdata'])->name('insertdata');
Route::get('/tampilkandata/{id}', [MatakuliahController::class, 'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}', [MatakuliahController::class, 'updatedata'])->name('updatedata');
Route::get('/deletedata/{id}', [MatakuliahController::class, 'deletedata'])->name('deletedata');

//login Matakuliah
Route::get('/prelogin', [loginController::class, 'prelogin'])->name('prelogin');
Route::get('/login', [loginController::class, 'login'])->name('login');
Route::get('/loginUser', [loginController::class, 'loginUser'])->name('loginUser');
Route::get('/register', [loginController::class, 'register'])->name('register');
Route::get('/registerUser', [loginController::class, 'registerUser'])->name('registerUser');
Route::get('/lupapass', [loginController::class, 'lupapass'])->name('lupapass');
Route::get('/lupapassUser', [loginController::class, 'lupapassUser'])->name('lupapassUser');


Route::get('/jadwalmk', [JadwalController::class,'index'])->name('jadwalmk');
Route::get('/tambahjadwal', [JadwalController::class,'tambahjadwal'])->name('tambahjadwal');
Route::post('/insertjadwal', [JadwalController::class,'insertjadwal'])->name('insertjadwal');
Route::get('/tampilkandata/{id}', [JadwalController::class,'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}', [JadwalController::class,'updatedata'])->name('updatedata');
Route::get('/delete/{id}', [JadwalController::class,'delete'])->name('delete');

Route::get('/Dosen', [DosenController::class,'tabelDosen']);
Route::get('/createDosen', [DosenController::class,'createDosen']);
Route::post('/storeDosen',[DosenController::class,'storeDosen']);
Route::get('/showDosen/{id}', [DosenController::class,'showDosen']);
Route::post('/updateDosen/{id}', [DosenController::class,'updateDosen']);
Route::get('/destroyDosen/{id}', [MDosenController::class,'destroyDosen']);
