<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->primary('id')->length(10);
            $table->integer('NIM')->length(10);
            $table->foreign('NIM')->references('NIM')->on('mahasiswas')->onDelete('cascade');
            $table->integer('id_matkul')->length(10);
            $table->foreign('id_matkul')->references('id_matkul')->on('matkuls')->onDelete('cascade');
            $table->integer('nilai');
            $table->string('komentar', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilais');
    }
}
