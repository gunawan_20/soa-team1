<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\jurusans;

class jurusan extends Controller
{
    function index(){
        $data = array(
            "jurusan" => jurusans::get()
        );
        return view('jurusan', $data);
    }

    function tambah(){
        return view('jurusan_tambah');
    }

    function proses_tambah(Request $request){
        $request->validate([
            'jurusan' => "required"
        ],[
            "required" => "Nama Jurusan Tidak Boleh Kosong!"
        ]);

        $jurusan = new jurusans();
        $jurusan->nama_jurusan = $request->jurusan;
        if($jurusan->save()){
            return redirect('/jurusan')->with('sukses','Berhasil Menambahkan Jurusan!');
        }
        return redirect('/jurusan')->with('gagal','Gagal Menambahkan Jurusan');
    }

    function edit($id){
        $data = array(
            "jurusan" => jurusans::where('id_jurusan',$id)->first()
        );
        return view('jurusan_edit', $data);
    }

    function proses_edit(Request $request){
        $request->validate([
            "jurusan" => "required"
        ],[
            "required" => ":Attribute Tidak Boleh Kosong!"
        ]);

        if(jurusans::where('id_jurusan',$request->id)->update(['nama_jurusan' => $request->jurusan])){
            return redirect()->route('jurusan')->with('sukses','Update Berhasil');
        }
        return redirect()->route('jurusan')->with('gagal','Update Gagal');
    }

    function hapus($id){
        if(jurusans::where('id_jurusan',$id)->delete()){
            return redirect()->route('jurusan')->with('sukses','Hapus Berhasil');
        }
        return redirect()->route('jurusan')->with('gagal','Hapus Gagal');
    }
}
