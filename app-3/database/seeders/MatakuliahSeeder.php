<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MatakuliahSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('matakuliahs')->insert([
            'id' => '1122',
            'id_dosen' => '1355',
            'nama_matkul' => 'Rekayasa Perangkat Lunak',
            'SKS' => '3',
        ]);
    }
}
