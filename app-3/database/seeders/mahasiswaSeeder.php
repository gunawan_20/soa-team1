<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class mahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mahasiswas')->insert([
            'nama' => 'Gunawan',
            'jenisKelamin' => 'Laki-Laki',
            'NIM' => 4611,
            'alamat'=> 'Cilacap'
        ]);
    }
}
