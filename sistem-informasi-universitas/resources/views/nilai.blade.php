@extends('master')

    
@section('konten')

<h3>DATA NILAI MAHASISWA</h3>
<a href="/tambahNilai"><button type="button" class="btn btn-primary mt-4 mb-2">Tambah Data Nilai</button></a>
<div class="input-group mb-3">
  <form action="/nilai" method="get">
    <input type="search" name ="search" class="form-control" placeholder="Cari Berdasarkan Nilai" >
  </form>
</div>
<div class="card mt-3" style="width: 98%;">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">NO</th>
            <th scope="col">MAHASISWA</th>
            <th scope="col">NIM</th>
            <th scope="col">MATAKULIAH</th>
            <th scope="col">ID MATAKULIAH</th>
            <th scope="col">NILAI</th>
            <th scope="col">KOMENTAR</th>
            <th scope="col">EDIT</th>
          </tr>
        </thead>
        <tbody>
          @php
            $no=1;  
          @endphp
            @foreach ($data as $index => $baris)
    
            <tr>
                <th scope="row">{{ $index+$data->firstItem() }}</th>
                <td>{{ $baris->mahasiswa['nama_mhs'] }}</td>
                <td>{{ $baris->mahasiswa['NIM'] }}</td>
                <td>{{ $baris->matkul['nama_matkul'] }}</td>
                <td>{{ $baris->matkul['id_matkul'] }}</td>
                <td>{{ $baris->nilai }}</td>
                <td>{{ $baris->komentar }}</td>
                <td> 
                  <a href="/tampilNilai/{{ $baris->id }}" class="btn btn-success">Edit</a>
                  <a href="#" class="btn btn-danger delete" data-id="{{ $baris->id }}" data-nama="{{ $baris->mahasiswa->nama_mhs }}" >Delete</a>
                </td>
            </tr>
                
            @endforeach
        </tbody>
        {{ $data->links() }}
      </table>
  </div>

  @section('sweetalert')
    <script>
      $('.delete').click(function(){
        var nilaiid = $(this).attr('data-id');
        var nama = $(this).attr('data-nama');
            
        swal({
        title: "Anda yakin ingin menghapusnya?",
        text: "Anda akan menghapus data nilai mahasiswa  "+nama+" ",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
          if (willDelete) {
            window.location = "/deleteNilai/"+nilaiid+""
            swal("Data berhasil dihapus!", {
              icon: "success",
            });
          } else {
            swal("Data batal dihapus");
          }
        });
      });
    </script>
  @endsection
    
@endsection