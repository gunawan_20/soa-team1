<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    use HasFactory;
    protected $table = "nilais";
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function mahasiswa(){
        return $this->belongsTo(Mahasiswa::class, 'NIM');
    }

    public function matkul(){
        return $this->belongsTo(Matkul::class, 'id_matkul');
    }
}
