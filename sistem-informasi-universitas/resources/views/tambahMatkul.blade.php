@extends('master')
    
@section('konten')
<h2 class="mb-3 d-flex justify-content-center">Tambah Data Matakuliah</h2>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-7">
          <div class="card">
              <div class="card-body">
                            <form action="/insertDataMatkul" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="mb-3">
                                    <label for="id_matkul" class="form-label">ID</label>
                                    <input type="text" name="id_matkul" class="form-control"  id="id_matkul" >
                                </div>
                                <div class="mb-3">
                                    <label for="nama_matkul" class="form-label">MATAKULIAH</label>
                                    <input type="text" name="nama_matkul" class="form-control"  id="nama_matkul" >
                                </div>
                                <div class="mb-3">
                                  <label for="sks" class="form-label">SKS</label>
                                  <input type="number" class="form-control" name ="sks" id="sks">
                                </div>
                                <button type="submit" class="btn btn-primary login">Tambah</button>
                              </form>
                              <a href="/matakuliah" id="kembali"><button type="submit" class="btn btn-danger mt-2">Kembali</button></a>
                        </div>
                  </div>
              </div>
          </div>
      </div>
@endsection