@extends('master')


@section('konten')
<title>Info Jadwal</title>
  </head>
  <body>
    <h2 class="text-center mb-4 mt-5">JADWAL MATAKULIAH</h2>
    
   <div class="container">
       <div class="row">
        <table class="table mt-3">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Id Jadwal</th>
                <th scope="col">NIP Dosen</th>
                <th scope="col">Nama MK</th>
                <th scope="col">Hari</th>
                <th scope="col">Jam Awal</th>
                <th scope="col">Jam Selesai</th>
                <th scope="col">Id Gedung</th>
              </tr>
            </thead>
            <tbody>
              @php
                $id=1;  
              @endphp
            @foreach ($data as $index=>$datajadwal)
              <tr>
                <th scope="row">{{ $index+$data->firstItem() }}</th>
                <td>{{ $datajadwal->id_jadwal }}</td>
                <td>{{ $datajadwal->id_dosen }}</td>
                <td>{{ $datajadwal->nama_mk }}</td>
                <td>{{ $datajadwal->hari }}</td>
                <td>{{ $datajadwal->jam_awal }}</td>
                <td>{{ $datajadwal->jam_selesai}}</td>
                <td>{{ $datajadwal->id_gedung }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
          {{ $data->links() }}
       </div>
       <a href="/user" class="btn btn-dark mt-5 ">Kembali </a>
   </div>

@endsection
