<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Halaman Matakuliah</title>
  </head>
  <body>
    <h1 class="text-center mb-2 mt-4">Tabel Matakuliah</h1>
    <div class="container mb-4">
        <a type="button" class="btn btn-primary mb-3" href="/tambahdata">+Tambah</a> 
        <div class="row g-3 align-items-center">
            <div class="col-auto">
                <form action="/matakuliah" method="GET">    
                 <input type="search" id="inputPassword6" name="search" class="form-control" aria-describedby="passwordHelpInline">
                </form>
            </div>
        </div>
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Id Matkul</th>
                        <th scope="col">Id Dosen</th>
                        <th scope="col">Nama Matkul</th>
                        <th scope="col">SKS</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($data as $index => $baris)  
                        <tr>
                            <th scope="row">{{ $index + $data->firstItem() }}</th>
                            <td>{{ $baris->id }}</td>
                            <td>{{ $baris->id_dosen }}</td>
                            <td>{{ $baris->nama_matkul }}</td>
                            <td>{{ $baris->SKS }}</td>
                            <td>
                                <a href="/deletedata/{{ $baris->id }}" class="btn btn-danger">Hapus</a>
                                <a href="/tampilkandata/{{ $baris->id }}}" class="btn btn-info" >Edit</a>
                            </td>
                        </tr>  
                    @endforeach
                </tbody>
            </table>
            {{ $data->links() }}
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


  </body>
</html>