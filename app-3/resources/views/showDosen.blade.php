@extends('layouts.default')

@section('content')
<section>
    <div class="container mt-5">
        <h1> Edit Dosen </h1>
        <div class="row">
            <div class="col-lg-8">
                <form action="{{url('/updateDosen/'. $data->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="id_dosen"> ID Dosen </label>
                        <input type"number" name="id_dosen" class="form-control" placeholder="12345" value="{{$data->id_dosen}}">
                    </div> 
                    <div class="form-group">
                        <label for="id_jurusan"> ID Jurusan </label>
                        <input type"number" name="id_jurusan" class="form-control" placeholder="12345" value="{{$data->id_jurusan}}">
                    </div>
                    <div class="form-group">
                        <label for="nama_dosen"> Nama Dosen </label>
                        <input type"text" name="nama_dosen" class="form-control" placeholder="Ahmad Roni" value="{{$data->nama_dosen}}">
                    </div>
                    <div class="form-group">
                        <label for="email"> Email </label>
                        <input type"form-control" name="alamat" class="form-control" placeholder="contohemail@email.com" value="{{$data->email}}">
                    </div>
                    <div class="form-group">
                        <label for="alamat"> Alamat </label>
                        <input type"form-control" name="alamat" class="form-control" placeholder="Indonesia" value="{{$data->alamat}}">
                    </div>

                        <div class="form-group mt-2">
                            <button type="submitDosen" class="btn btn-primary"> Edit Dosen </button>
                        </div>

                        <div class="form-group">
                            <a href="{{url('Dosen')}}"> << Kembali ke halaman utama </a>