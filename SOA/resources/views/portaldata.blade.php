@extends('branch')

@section('title')
<title> Dashboard Data </title>

@endsection

@section('External_CSS')
<!-- CSS Files -->
<link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.2" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('portaldata.css') }}">
<script src="http://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
  <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
  
@endsection

@section('konten')

<div class="container">
  <h2 class="content">Home</h2>
  <hr class="line">
</div>


<!-- Cards -->
<div class="container cardBox">
  <div class="d-flex justify-content-between">
    <div>
      <div class="user_numbers">{{ $jumlahpengguna }}</div>
      <div class="cardName">Pengguna Aktif</div>
    </div>
    <div class="iconBox">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
        <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0zM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816zM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275zM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4z"/>
      </svg>
    </div>
  </div>



  <div class="d-flex justify-content-between">
    <div>
      <div class="item_numbers">{{ $jumlahpesan }}</div>
      <div class="cardName">Pesan Keluar</div>
    </div>
    <div class="iconBox">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
        <path d="M13.5 3a.5.5 0 0 1 .5.5V11H2V3.5a.5.5 0 0 1 .5-.5h11zm-11-1A1.5 1.5 0 0 0 1 3.5V12h14V3.5A1.5 1.5 0 0 0 13.5 2h-11zM0 12.5h16a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 12.5z"/>
      </svg>
    </div>
  </div>

  <div class="d-flex justify-content-between">
    <div>
      <div class="money_numbers">{{ $jumlahtransaksi }}</div>
      <div class="cardName">Jumlah Transaksi</div>
    </div>
    <div class="iconBox">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-coin" viewBox="0 0 16 16">
        <path d="M5.5 9.511c.076.954.83 1.697 2.182 1.785V12h.6v-.709c1.4-.098 2.218-.846 2.218-1.932 0-.987-.626-1.496-1.745-1.76l-.473-.112V5.57c.6.068.982.396 1.074.85h1.052c-.076-.919-.864-1.638-2.126-1.716V4h-.6v.719c-1.195.117-2.01.836-2.01 1.853 0 .9.606 1.472 1.613 1.707l.397.098v2.034c-.615-.093-1.022-.43-1.114-.9H5.5zm2.177-2.166c-.59-.137-.91-.416-.91-.836 0-.47.345-.822.915-.925v1.76h-.005zm.692 1.193c.717.166 1.048.435 1.048.91 0 .542-.412.914-1.135.982V8.518l.087.02z"/>
        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
        <path d="M8 13.5a5.5 5.5 0 1 1 0-11 5.5 5.5 0 0 1 0 11zm0 .5A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"/>
      </svg>
    </div>
  </div>

  <div class="d-flex justify-content-between">
    <div>
      <div class="employ_numbers">{{  $jumlahkaryawan  }}</div>
      <div class="cardName">Jumlah Karyawan</div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-person-check" viewBox="0 0 16 16">
      <path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
      <path fill-rule="evenodd" d="M15.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L12.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
    </svg>
  </div>
</div>

<div class="container mb-5">
  <h2 class="content2">Statistik Perusahaan</h2>
  <hr class="line-bottom">
         <div class="card mt-5 pt-5">
          <div class="col">
                    <div class = "mx-auto pt-5" id="container" style="width: 80%;">
                        <p class="text-center"><b>Penjualan : Berdasarkan Banyaknya Barang Terjual</b></p>
                        <p class="text-center">Per Bulan</p>
                        <canvas id="canvas3"></canvas>
                    </div>
                </div>
                <script>
                const data = {
                labels: <?php echo json_encode($Months);?>,
                datasets: [{
                    label: 'Jumlah Barang Terjual',
                    data: <?php echo json_encode($Jumlah_barang_terjual_perbulan);?>,
                    borderColor: 'rgb(255, 99, 132)',
                    backgroundColor: [
                        'rgba(123, 165, 254, 1)',  
                        'rgba(123, 165, 254, 1)', 
                        'rgba(123, 165, 254, 1)',  
                        'rgba(240, 117, 126, 1)',
                        'rgba(245, 181, 81, 1)',
                        'rgba(66, 184, 245, 1)',
                        'rgba(77, 199, 181, 1)',
                        'rgba(240, 117, 126, 1)'
                    ],
                }]
                };
                const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                    y: {
                        beginAtZero: true
                    }
                    }
                }
                };
            </script>    
            <script>
                const penjualanChart = new Chart(
                document.getElementById('canvas3'),
                config
                );
            </script>

            <div class="container pt-4 mb-3">
    
        
        <div class="row">
            <div class="col">
                <div class = "mx-auto pt-5" id="container" style="width: 60%;">
                    <p class="text-center"><b>Pegawai : Berdasarkan Jenis Kelamin</b></p>
                    <canvas id="canvas2"></canvas>
                </div>
            </div>
          </div>
          
          <script>
            
            const jenisKelamin = <?php echo json_encode($JenisKelamin)?>;
          
            const dataJenisKelamin = {
              labels: jenisKelamin,
              datasets: [{
                label: 'My First dataset',
                backgroundColor: [
                      'rgba(123, 165, 254, 1)',
                      'rgba(77, 199, 181, 1)'
                      ],
                data: <?php echo json_encode($JumlahSetiapJenisKelamin);?>,
              }]
            };
          
            const configJenisKelamin = {
              type: 'pie',
              data: dataJenisKelamin,
              options: {}
            };
              </script>
          <script>
              const jenisKelaminChart = new Chart(
                document.getElementById('canvas2'),
                configJenisKelamin
              );
          </script>
      </div>
@endsection