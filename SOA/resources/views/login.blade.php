<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LOGIN | ERP DATAHOUSE</title>

    <link  href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('stylelogin.css') }}">
</head>
<body>
    
        <div class="row">
            <div class="col-5">
              <div class="kolom1">
                <h2>LOGIN</h2>
                <a href="{{ url('auth/google') }}"><button type="button" class="btn btn-danger tombol tombol1">
                  <img src="{{ asset('images/icon_google.png') }}" width="20px" height="20px">
                    Sign in with Google
                </button>
                </a>
                <hr width="400px" >
                <p class="or">or Sign in with Email</p>
                <form method="POST" action="/login/kirim">
                  @csrf
                    <div class="mb-3">
                      <label for="email" class="form-label">Email</label>
                      <input type="email" class="form-control tombol" name="email" id="email" placeholder="mail@unnes.ac.id">
                    </div>
                    <div class="mb-3">
                      <label for="password" class="form-label">Password</label>
                      <input type="password" class="form-control tombol" name="password"id="password" placeholder="min 8 character">
                    </div>
                    <div class="mb-3 form-check">
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                      <label class="form-check-label" for="exampleCheck1">Remember me</label>
                      <a href="#">Forgot password?</a>
                    </div>
                    <button type="submit" class="btn btn-primary tombol">LOGIN</button>
                  </form>
                  <b>Copyright@ TEAM 1</b>
                </div>
            </div>
            <div class="col-7 kolom2">
              
              <img src="{{ asset('images/site.png') }}" style="margin:auto" class="gudang" alt="" srcset="" width="520px" height="550px">
            </div>
        </div>
    
    
</body>
</html>