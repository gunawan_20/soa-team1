@extends('layouts.default')

@section('content')
    <div class="container">
    	<div class="row">
    		<div class="col-lg-8">
    			<h1>CRUD DOSEN</h1>
    			<a href="{{url('/createDosen')}}" class="btn btn-primary"> +Tambah Dosen </a>
    		</div>

            <div class="col-lg-8 mt-5">
                <table class="table-bordered">
                    <tr>
                        <th>ID Dosen</th>
                        <th>ID Jurusan</th>
                        <th>Nama Dosen</th>
                        <th>Email</th>
                        <th>Alamat</th>
                    </tr>
                    @foreach ($data as $dataDosen)
                        <tr>
                            <td>{{$dataDosen->id_dosen}}</td>
                            <td>{{$dataDosen->id_jurusan}}</td>
                            <td>{{$dataDosen->nama_dosen}}</td>
                            <td>{{$dataDosen->email}}</td>
                            <td>{{$dataDosen->alamat}}</td>
                            <td>
                                <a href="{{url('/showDosen/'. $dataDosen->id)}}" class="btn btn-warning">Edit</a>
                                <a href="{{url('/destroyDosen/'. $dataDosen->id)}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach    
                </table>
    	</div>
    </div>
@endsection