<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    use HasFactory;

    protected $guarded=[];
    public $timestamps = false;
    protected $primaryKey = 'id_jadwal';
    // protected $dates =['create_at'];

    
    public function dosen(){
        return $this->hasMany(id_dosen::class);
    }
}

