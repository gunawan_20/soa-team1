<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    use HasFactory;
    protected $guarded=[];
    public $timestamps = false;
    protected $primaryKey = 'id_dosen';


    public function nilai(){
        return $this->hasMany(Nilai::class);
    }
}
