@extends("masterdashboard")

@section('title')
<title> Inventaris </title>
@endsection
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
@section('External_CSS_JS')

@endsection

@section("judul_laman")
<div class="text"> Inventaris </div>
@endsection

@section("konten")

<!-- Jangan lupa bungkus dengan "container" -->
<div class="container">
<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Kategori_Barang</th>
        {{-- <th scope="col">Kode_Barang</th>
        <th scope="col">Nama_Barang</th>
        <th scope="col">Stok_Barang</th>
        <th scope="col">Harga_Barang</th> --}}
      </tr>
    </thead>
    <tbody>
    @foreach ()
    @for ($i = 0; $i < $dataKategori; $i++)
    @for ($j = 0; $j < $dataBarang; $j++)
    <tr>
        <td>{{ $ketegori[$i] }}</td>
        <td>{{ $kode_barang[$j] }}</td>
        <td>{{ $nama_barang[$j] }}</td>
        <td>{{ $stok[$j] }}</td>
        <td>{{ $harga[$j] }}</td>
    </tr>
    @endfor 
    @endfor 
    </tbody>
  </table>
</div>

@endsection