<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function dataDosen(Request $request)
    {
        if ($request->has('search')) {
            $data = Dosen::where('nama_dosen','LIKE','%'.$request->search.'%')->paginate(5);
        }else {
            $data = Dosen::paginate(5);
        }

        return view('profildosen',compact('data'));

    }
    public function tambahDosen(){
        
        $data = Dosen::all();
        return view('tambahDosen', compact('data'));

    }
    public function insertDosen(Request $request){
        
        Dosen::create($request ->all());
        return redirect()->route('profildosen');
    }

    public function tampildosen($id_dosen){
        
        $data = Dosen::find($id_dosen);
        //dd($data);

        return view('tampildosen', compact('data'));
    }

    public function updateDosen(Request $request, $id_dosen){
        
        $data = Dosen::find($id_dosen);
        //dd($data);
        $data->update($request->all());
        return redirect()->route('profildosen')->with('success','Data Berhasil Di Ubah!');
    }

    public function jadwalDosen(Request $request)
    {
        if ($request->has('search')) {
            $data = Dosen::where('nama_dosen','LIKE','%'.$request->search.'%')->paginate(5);
        }else {
            $data = Dosen::paginate(5);
        }

        return view('jadwaldosen',compact('data'));

    }

    public function deletedosen($id_dosen){
        
        $data = Dosen::find($id_dosen);
        //dd($data);
        $data->delete();
        return redirect()->route('profildosen')->with('success','Data Berhasil Di Hapus!');
    }

    
}
