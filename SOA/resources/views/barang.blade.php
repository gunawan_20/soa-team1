@extends("master")

@section('title')
<title> Data Barang</title>
@endsection

@section('External_CSS')
<link rel="stylesheet" href="{{ asset('barang.css') }}">
@endsection

@section("konten")
  {{-- <link rel="stylesheet" href="{{ asset('barang.css') }}"> --}}
  <script src="http://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
  <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  

  {{-- HTML --}}
  <div class="container mb-3">
    <h4 class="pt-5">Manajemen Barang</h4>
    <p class="mb-4">Sumber Data : Sistem Informasi Barang</p>
  <div class="bungkus">
    <div id="container" class="batang">
        <p style="text-align: center"><b>Sebaran Jumlah Barang</b></p>
        <p class="text-center">Per Kategori</p>
    <canvas id="canvas"></canvas>
    </div>
    <div class="pie">
      <p style="text-align: center"><b>Jumlah Barang Paling Banyak dan Paling Sedikit</b></p>
      <p class="text-center">Per Kategori</p>
            <canvas id="myChart"></canvas>
    </div>
    <div class="line">
      <p style="text-align: center"><b>Range Harga Barang</b></p>
      <p class="text-center">Per Kategori</p>
      <canvas id="myChart1"></canvas>
     </div> 
  </div>
 {{-- JAVASCRIPT --}}
<script>
        // Diagram Batang
        var chartdata = {
        type: 'bar',
        data: {
        labels: <?php echo json_encode($Kategori); ?>,
        datasets: [
        {
        label: 'Stok Brang',
        backgroundColor: [
          '#F0757E',
          '#F5B551',
          '#4DC7B5',
          '#42B8F5',
        ],
        borderWidth: 1,
        data: <?php echo json_encode($Stock); ?>
        }]},
        options: {
        scales: {
        yAxes: [{
        ticks: {
        beginAtZero:true
        }}]}}}
        var ctx = document.getElementById('canvas').getContext('2d');
        new Chart(ctx, chartdata);
        var oilCanvas = document.getElementById("oilChart");
        // var pie_basic_element = document.getElementById('pie_basic');
//  Pie
const data = {
  labels:<?php echo json_encode($Kategori1); ?>,
  datasets: [{
    label: 'My First Dataset',
    data:  <?php echo json_encode($bk); ?>,
    backgroundColor: [
      '#F0757E',
      '#42B8F5',
      '#F5B551',
      '#4DC7B5',
      '#F7D716',
      '#F6E3CH'
    ],
    hoverOffset: 4
  }]};
    const config = {
    type: 'pie',
    data: data,
  };
    const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

// HARGA BARANG
window.onload = function() {
         var ctx = document.getElementById('myChart1').getContext('2d');
         window.myBar = new Chart(ctx, {
            type: 'bar',
            data:{
               labels:<?php echo json_encode($Kategori); ?>,
               datasets:[{
                 label:'Range Harga',
                 data:<?php echo json_encode($harga); ?>,
                 backgroundColor:'rgba(255,99,132, 0.6)'
               }]
            },
            options: {
               responsive: true,
               legend: {
                 position: 'top',
               },
               title: {
                 display: true,
                 text: 'Chart.js Bar Chart'
               }
            }
         });
      };
</script>
</div>
@endsection