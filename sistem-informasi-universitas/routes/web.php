<?php

use App\Http\Controllers\AuthenticationController;
use App\Models\Matkul;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\MatkulController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\MahasiswaController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//LOGIN
Route::get('/',[LoginController::class,'login'])->name('login');
Route::post('/login/kirim',[LoginController::class,'loginUser'])->name('loginaksi');



//dashboard
Route::group(['middleware' => ['auth']],function(){
    Route::get('/dashboard', function(){
        return view('dashboard');
    })->name('dashboard');

    
Route::get('/logOut',[LoginController::class,'logOut'])->name('Log Out');
});


//Middleware

Route::group(['middleware' =>['auth','hakAkses:admin']], function(){

     //USER
    Route::get('/user',[UserController::class,'dataUser'])->name('Data User');
    Route::get('/user/tambah',[UserController::class,'tambahUser'])->name('Tambah User');
    Route::post('user/tambah/insert', [UserController::class,'insertDataUser'])->name('Insert Data User');
    Route::get('/deleteUser/{id}',[UserController::class,'deleteUser']);
    Route::get('/updatedatauser/{id}',[UserController::class,'updatedatauser']);
    Route::post('/updatebyid/{id}',[UserController::class,'updateUser']);

    

    //MATAKULIAH
    Route::get('/matakuliah',[MatkulController::class,'dataMatkul'])->name('Data Matkul');
    Route::get('/tambahMatkul',[MatkulController::class,'tambahMatkul'])->name('tambahMatkul');
    Route::post('/insertDataMatkul',[MatkulController::class,'insertDataMatkul'])->name('insertDataMatkul');
    Route::get('/tampilMatkul/{id_matkul}',[MatkulController::class,'tampilMatkul'])->name('tampilMatkul');
    Route::post('/editMatkul/{id_matkul}',[MatkulController::class,'editMatkul'])->name('editMatkul');
    Route::get('/deleteMatkul/{id_matkul}',[MatkulController::class,'deleteMatkul'])->name('deleteMatkul');
    
});

Route::Group(['middleware'=>['auth','hakAkses:admin,dosen']],function(){
    //dosen
    Route::get('/profildosen', [DosenController::class,'dataDosen'])->name('profildosen');
    Route::get('/tambahDosen', [DosenController::class,'tambahDosen'])->name('tambahDosen');
    Route::get('/jadwaldosen', [DosenController::class,'jadwalDosen'])->name('jadwaldosen');
    Route::post('/insertDosen',[DosenController::class, 'insertDosen'])->name('insertDosen');
    Route::get('/tampildosen/{id}',[DosenController::class, 'tampildosen'])->name('tampildosen');
    Route::post('/updateDosen/{id}',[DosenController::class, 'updateDosen'])->name('updateDosen');
    Route::get('/deletedosen/{id}',[DosenController::class, 'deletedosen'])->name('deletedosen');

    //NILAI
    Route::get('/nilai',[NilaiController::class,'dataNilai'])->name('Data Nilai');
    Route::get('/tambahNilai',[NilaiController::class,'tambahNilai'])->name('tambahNilai');
    Route::post('/insertDataNilai',[NilaiController::class,'insertDataNilai'])->name('insertDataNilai');
    Route::get('/tampilNilai/{id}',[NilaiController::class,'tampilNilai'])->name('tampilNilai');
    Route::post('/editNilai/{id}',[NilaiController::class,'editNilai'])->name('editNilai');
    Route::get('/deleteNilai/{id}',[NilaiController::class,'deleteNilai'])->name('deleteNilai');
  
});

//akademik
Route::get('/akademik', [JadwalController::class,'akademik'])->name('akademik');

//jadwal matkul
Route::get('/tambahjadwal', [JadwalController::class,'tambahjadwal'])->name('tambahjadwal');
Route::post('/insertjadwal', [JadwalController::class,'insertjadwal'])->name('insertjadwal');
Route::get('/tampilkandata/{id}', [JadwalController::class,'tampilkandata'])->name('tampilkandata');
Route::post('/updatedata/{id}', [JadwalController::class,'updatedata'])->name('updatedata');
Route::get('/delete/{id}', [JadwalController::class,'delete'])->name('delete');
Route::get('/infojadwal', [JadwalController::class,'infojadwal'])->name('infojadwal');



//mahasiswa
Route::get('/datamahasiswa',[MahasiswaController::class, 'index'])->name('mahasiswa');
Route::get('/tambahMahasiswa',[MahasiswaController::class, 'tambahMahasiswa'])->name('tambahMahasiswa');
Route::post('/insertmahasiswa',[MahasiswaController::class, 'insertmahasiswa'])->name('insertmahasiswa');

Route::get('/tampilmahasiswa/{id}',[MahasiswaController::class, 'tampilmahasiswa'])->name('tampilmahasiswa');
Route::post('/updatemahasiswa/{id}',[MahasiswaController::class, 'updatemahasiswa'])->name('updatemahasiswa');

Route::get('/deletemahasiswa/{id}',[MahasiswaController::class, 'deletemahasiswa'])->name('deletemahasiswa');

//Nilai di Mahasiswa
Route::get('/mhsNilai',[NilaiController::class,'mhsNilai'])->name('mhsNilai');





Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/auth/google',[AuthenticationController::class,'redirect']);
Route::get('/auth/callback',[AuthenticationController::class,'callback']);
