@extends('master')

    
@section('konten')

<h3>DATA MATAKULIAH</h3>
<a href="/tambahMatkul"><button type="button" class="btn btn-primary mt-4 mb-2">Tambah Matakuliah</button></a>
<div class="input-group mb-3">
  <form action="/matakuliah" method="get">
    <input type="search" name ="search" class="form-control" placeholder="Cari Matakuliah" >
  </form>
</div>
<div class="card mt-3" style="width: 98%;">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">ID MATKUL</th>
            <th scope="col">MATAKULIAH</th>
            <th scope="col">SKS</th>
            <th scope="col">EDIT</th>
          </tr>
        </thead>
        <tbody>
            
            @foreach ($data as $index => $baris)
    
            <tr>
                <td>{{ $baris->id_matkul }}</td>
                <td>{{ $baris->nama_matkul }}</td>
                <td>{{ $baris->sks }}</td>
                <td> 
                  <a href="/tampilMatkul/{{ $baris->id_matkul }}" class="btn btn-success">Edit</a>
                  <a href="#" class="btn btn-danger delete" data-id="{{ $baris->id_matkul }}" data-nama="{{ $baris->nama_matkul }}">Delete</a>
                </td>
            </tr>
                
            @endforeach
        </tbody>
        {{ $data->links() }}
      </table>
  </div>

  @section('sweetalert')
    <script>
      $('.delete').click(function(){
        var matkulid = $(this).attr('data-id');
        var nama = $(this).attr('data-nama');
            
        swal({
        title: "Anda yakin ingin menghapusnya?",
        text: "Anda akan menghapus data matakuliah "+nama+" ",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
          if (willDelete) {
            window.location = "/deleteMatkul/"+matkulid+""
            swal("Data berhasil dihapus!", {
              icon: "success",
            });
          } else {
            swal("Data batal dihapus");
          }
        });
      });
    </script>
  @endsection
    
@endsection