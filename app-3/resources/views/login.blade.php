<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
      .txt1{
        float: right;
      }
    </style>
    <title>Login Admin</title>
  </head>
  <body>
    <h1 class="text-center mb-4 mt-4">Sign In Administrator</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <form action="/login" method="GET" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                              <label for="exampleInputPassword1" class="form-label">Email</label>
                              <input type="text" name="id" class="form-control" id="exampleInputPassword1">
                            </div>
                            <div class="mb-3">
                              <label for="exampleInputPassword1" class="form-label">Password</label>
                              <input type="password" name="SKS" class="form-control" id="exampleInputPassword1">
                            </div>
                            
                            <div class="flex-sb-m w-full p-t-3 p-b-24">
                              <div class="contact100-form-checkbox">
                                <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                                <label class="label-checkbox100" for="ckb1">
                                  Ingat saya
                                </label>
                                <span>
                                  <a href="/lupapass" class="txt1">
                                    Lupa Password?
                                  </a>
                                </span>
                              </div>
                            </div>
                            <div>
                              <center>
                                <div class="container-login100-form-btn m-t-17">
                                  <a href="/matakuliah" class="btn btn-primary mt-4 ">Masuk</a>
                                  <br>
                                </div>
                                <br>
                                Belum memiliki akun?
                                <br>
                                <div class="container-login100-form-btn m-t-17">
                                  <a href="/register" class="btn btn-secondary mt-3 " >Daftar</a>
                                </div>
                              </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


  </body>
</html>