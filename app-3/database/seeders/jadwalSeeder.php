<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class jadwalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jadwals')->insert([
        'id_jadwal' => '123',
        'id_dosen' => '4242',   
        'nama_mk' => 'Rekayasa software',   
        'hari' => 'kamis',   
        'jam_awal' => '07:00:00',   
        'jam_selesai' => '10:00:00', 
        'id_gedung' => 'D5',   
        ]);
    }
}
