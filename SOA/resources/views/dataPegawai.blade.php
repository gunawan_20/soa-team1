@extends("masterdashboard")

@section('title')
<title> Data Pegawai </title>
@endsection

@section('External_CSS_JS')

@endsection

@section("judul_laman")
@endsection

@section("konten")

<!-- Jangan lupa bungkus dengan "container" -->
<div class="container mx-auto mb-4" style="width: 90%;">
  <h4 class="pt-2">TABEL DATA PEGAWAI</h4>
        <p class="mb-4">Sumber Data : Sistem Informasi Pengembangan Sumber Daya</p>
  <div class="card">
      <div class="row">
          <div class="col">
              <div class ="mx-auto pt-5 pb-5" id="container" style="width: 80%;">
                  <div class="container cardBoxHasil">
                      {{-- <h4 class="text-center mb-5">TABEL DATA PEGAWAI</h4> --}}
                            <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">NAMA</th>
                                    <th scope="col">EMAIL</th>
                                    <th scope="col">ALAMAT</th>
                                    <th scope="col">NO TELP</th>
                                    <th scope="col">UMUR</th>
                                    <th scope="col">JENIS KELAMIN</th>
                                  </tr>
                                </thead>
                                <tbody class="table-group-divider border-primary">
                                    
                                    @for ($i = 0; $i < $jumlah; $i++)
                                    <tr>
                                        <td>{{ $nama[$i] }}</td>
                                        <td>{{ $email[$i] }}</td>
                                        <td>{{ $alamat[$i] }}</td>
                                        <td>{{ $no_telp[$i] }}</td>
                                        <td>{{ $umur[$i] }}</td>
                                        <td>{{ $gender[$i] }}</td>
                                    </tr>
                                    @endfor
                                    
                                </tbody>
                              </table>
                         </div>
                    </div>
               </div>
          </div>
      </div>
</div>

@endsection