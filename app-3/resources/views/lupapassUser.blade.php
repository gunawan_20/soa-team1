<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Lupa Password</title>
  </head>
  <body>
    <h1 class="text-center mb-4 mt-4">Konfirmasi Email</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        <form action="#" method="GET" enctype="multipart/form-data">
                            @csrf
                            <div>
                              <center>
                                <p class="note mt-2">
                                  <h4>Perhatian</h4>
                                  Masukkan alamat Email yang telah terdaftar untuk mengirim notifikasi pembaharuan password
                                </p>
                              </center>
                            </div>
                            <div class="mb-3">
                              <label for="exampleInputPassword1" class="form-label">Email</label>
                              <input type="text" name="id" class="form-control" id="exampleInputPassword1">
                            </div>
                           
                            <div>
                              <center>
                                <div class="container-login100-form-btn m-t-17">
                                  <button type="submit" class="btn btn-primary mt-3 justify-center">Submit</button>
                                </div>
                                <br>
                                <div>
                                  <p>
                                    Notifikasi Password baru telah dikirim lewat email, silakan kembali ke halaman login untuk masuk
                                  </p>
                                </div>
                                <div class="container-login100-form-btn m-t-17">
                                  <a href="/loginUser" class="btn btn-secondary mt-3 " >Masuk</a>
                                </div>
                              </center>
                            </div>
                          </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


  </body>
</html>