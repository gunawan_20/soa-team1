@extends('master')
    
@section('konten')
<h2 class="mb-3 text-center">Tambah User</h2>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-8">
<div class="card" >
  <div class="card-body">
    <form action="/user/tambah/insert" method="POST">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Nama</label>
            <input type="text" name="name" class="form-control"  id="name" >
          </div>
        <div class="mb-3">
          <label for="email" class="form-label">Email</label>
          <input type="email" class="form-control" name ="email" id="email">
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">Password</label>
          <input type="password" class="form-control" name ="password" id="password">
        </div>
        <div class="mb-3">
        <select class="form-select" name ="role" id="role">
            <option selected>Pilih Role</option>
            <option value="admin">Admin</option>
            <option value="dosen">Dosen</option>
            <option value="mahasiswa">Mahasiswa</option>
          </select>
        </div>
        <button type="submit" class="btn btn-primary login">Tambah</button>
      </form>
      <a href="/user" id="kembali"><button type="submit" class="btn btn-danger d-inline mt-2">Kembali</button></a>
  </div>
</div>
</div>
</div>
</div>
@endsection