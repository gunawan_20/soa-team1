<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Session\Session;
use illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    //
    
    public function login()
    {
            return view('login');
    }
    public function dashboard()
    {
        return view('dashboard');
    }
   
    public function loginUser(Request $request)
    {
        $data =[
            'email' => $request->input('email'),
            'password'=>$request->input('password')
        ];
        if (Auth::attempt($data)) {
            return redirect('dashboard');
        }else {
            return redirect('/');
        }
    }
    public function logOut(){
        Auth::logout();
        return redirect('/');
    }
}

