<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gedung</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
</head>
<body>
    <div class="container">
        <div class="d-flex justify-content-center mt-5">
            <div class="col-md-4 col-md-offset-4">
                <h3 class="mb-3 text-center">Edit Gedung</h3>
                <form action="{{ route('proses-edit-gedung') }}" method="post">
                    <input type="text" name="id" value="{{ $gedung->id_gedung }}" hidden>
                    @csrf
                    <div class="form-group mt-3 mb-3">
                        <label>Nama Gedung</label>
                        <input type="text" name="gedung" class="form-control" value="{{ $gedung->nama_gedung }}">
                        <span class="text-danger">@error('gedung')
                            {{ $message }}
                        @enderror</span>
                    </div>
                    <div class="form-group mt-3 mb-3">
                        <label>Jurusan</label>
                        <select name="id_jurusan" class="form-control">
                            @foreach ($jurusan as $j)
                                @if ($j->id_jurusan == $gedung->id_jurusan)
                                    <option value="{{ $j->id_jurusan }}" selected>{{ $j->nama_jurusan }}</option>
                                @else
                                    <option value="{{ $j->id_jurusan }}">{{ $j->nama_jurusan }}</option>
                                @endif
                            @endforeach
                        </select>
                        <span class="text-danger">@error('id_jurusan')
                            {{ $message }}
                        @enderror</span>
                    </div>
                    <div class="form-group mt-3 mb-3">
                        <label>Kode Ruangan</label>
                        <input type="number" name="kd_ruangan" class="form-control" value="{{ $gedung->kd_ruangan }}">
                        <span class="text-danger">@error('kd_ruangan')
                            {{ $message }}
                        @enderror</span>
                    </div>
                    <div class="mt-4">
                        <button class="btn btn-success w-100">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
