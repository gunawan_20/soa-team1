@extends('master')

    
@section('konten')

<h3>DATA USER</h3>
<a href="/user/tambah"><button type="button" class="btn btn-primary mt-4 mb-2">Tambah User</button></a>
<div class="input-group mb-3">
  <form action="/user" method="get">
  <input type="search" name ="search" class="form-control" placeholder="Cari dengan email" >
</form>
</div>
<div class="card mt-3" style="width: 98%;">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">NAMA</th>
            <th scope="col">EMAIL</th>
            <th scope="col">ROLE</th>
            <th scope="col">EDIT</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($data as $users)
    
            <tr>
                <td>{{ $users->name }}</td>
                <td>{{ $users->email }}</td>
                <td>{{ $users->role }}</td>
                <td>
                  <a href="/updatedatauser/{{ $users->id }}"><button type="button" class="btn btn-success">Edit</button></a>
                  <a href="#" class="btn btn-danger delete" data-id="{{ $users->id }}" data-nama="{{ $users->name  }}">Delete</button></a>
                </td>
            </tr>
                
            @endforeach
        </tbody>
        {{ $data->links() }}
      </table>
  </div>

  @section('sweetalert')
  <script>
    $('.delete').click(function(){
      var userid = $(this).attr('data-id');
      var nama = $(this).attr('data-nama');
          
      swal({
      title: "Anda yakin ingin menghapusnya?",
      text: "Anda akan menghapus data user  "+nama+" ",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
          window.location = "/deleteUser/"+userid+""
          swal("Data berhasil dihapus!", {
            icon: "success",
          });
        } else {
          swal("Data batal dihapus");
        }
      });
    });
  </script>
@endsection

    
@endsection