<?php

namespace App\Http\Controllers;

use App\Models\Matkul;
use Illuminate\Http\Request;


class MatkulController extends Controller{
    public function dataMatkul(Request $request){
        if($request->has('search')){
            $data = Matkul::where('nama_matkul', 'LIKE', '%' . $request->search.'%')->paginate(5);
        }
        else{
            $data = Matkul::paginate(5);
        }
        return view('matkul', compact('data'));
    }

    public function tambahMatkul(){
        return view('tambahMatkul');
    }

    public function insertDataMatkul(Request $request){
        Matkul::create($request->all());
        return redirect()->route('Data Matkul');
    }

    public function tampilMatkul($id_matkul){
        $data = Matkul::find($id_matkul);
        return view('tampilMatkul', compact('data'));
    }

    public function editMatkul(Request $request, $id_matkul){
        $data = Matkul::find($id_matkul);
        $data->update($request->all());
        return redirect()->route('Data Matkul');
    }

    public function deleteMatkul($id_matkul){
        $data = Matkul::find($id_matkul);
        $data->delete();
        return redirect()->route('Data Matkul');
    }
    
}