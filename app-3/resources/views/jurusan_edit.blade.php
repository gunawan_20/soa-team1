<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jurusan</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
</head>
<body>
    <div class="container">
        <div class="d-flex justify-content-center mt-5">
            <div class="col-md-4 col-md-offset-4">
                <h3 class="mb-3 text-center">Tambah Jurusan</h3>
                <form action="{{ route('proses-edit-jurusan') }}" method="post">
                    @csrf
                    <input type="text" name="id" value="{{ $jurusan->id_jurusan }}" hidden>
                    <div class="form-group">
                        <label>Nama Jurusan</label>
                        <input type="text" name="jurusan" class="form-control" value="{{ $jurusan->nama_jurusan }}">
                        <span class="text-danger">@error('jurusan')
                            {{ $message }}
                        @enderror</span>
                    </div>
                    <div class="mt-4">
                        <button class="btn btn-success w-100">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
