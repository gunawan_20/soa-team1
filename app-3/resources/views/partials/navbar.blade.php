<nav class="navbar navbar-expand-lg navbar-dark bg-warning">
    <div class="container">
      <a class="navbar-brand fw-bold" href="#">UNNES Blog</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Link</a>
          </li>
        </ul>
        @if (Auth::user())
        <form action="{{ route('logout') }}" method="POST" class="d-flex">
          <button class="btn btn-light" type="submit"><i class="bi bi-box-arrow-right"></i> Logout</button>
          @csrf
        </form> 
        @else
         <a href="/login" class="btn btn-light center-block" type="submit"><i class="bi bi-box-arrow-right" onclick="window.location.href='{{ route('login') }}"></i> Login</a>
         @endif
        </div>
    </div>
  </nav>