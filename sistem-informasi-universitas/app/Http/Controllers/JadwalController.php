<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use Illuminate\Http\Request;
// use Illuminate\Support\Str;

class JadwalController extends Controller
{
    public function akademik(Request $request){
        if($request->has('search')){
            $data = Jadwal::where('nama_mk', 'LIKE', '%'.$request->search.'%')->paginate(2);
        }else{
            $data = Jadwal::simplepaginate(3);
        }
        return view('akademik',compact('data'));
    }

     public function tambahjadwal(){
       return view('tambahjadwal'); 
    }

    public function insertjadwal(Request $request){
        Jadwal::create($request->all());
        return redirect()->route('akademik')->with('success', 'Data Berhasil Ditambahkan');
    }

    public function tampilkandata($id){
        $data = Jadwal::find($id);
        //dd($data);
        return view('tampildata',compact('data'));
     }

     public function updatedata(Request $request, $id){
        $data = Jadwal::find($id);
        $data -> update($request->all());
        return redirect()->route('akademik')->with('success', 'Data Berhasil Di Update');
     }

     public function delete($id){
        $data = Jadwal::find($id);
        $data->delete();
        return redirect()->route('akademik')->with('success', 'Data Berhasil Di Delete');
     }
 

    public function infojadwal()
    {
        
        $data = Jadwal::simplepaginate(3);
        return view('infojadwal',compact('data'));
        // return view('nfojadwal',['data'=>$data]);
        // return view('infojadwal',['data'=>$data]);
    }

}

