<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('jurusans')->insert([
            [
                'nama_jurusan' => "Teknik Informatika"
            ],
            [
                'nama_jurusan' => "Sistem Informasi"
            ]
        ]);

        DB::table('gedungs')->insert([
            [
                'id_jurusan' => 1,
                'nama_gedung' => "Caplin",
                'kd_ruangan' => 1,
            ],
            [
                'id_jurusan' => 2,
                'nama_gedung' => "Charlie",
                'kd_ruangan' => 2,
            ]
        ]);
    }
}
