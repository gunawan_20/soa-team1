<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index(Request $request){
        if($request->has('search')){
            $data=Mahasiswa::where('nama_mhs','LIKE','%' .$request->search. '%')->paginate(5);
        }else{
            $data = Mahasiswa::paginate(5);
        }
        
        return view('datamahasiswa', compact('data'));

    }

    public function tambahMahasiswa(){
        
        $data = Mahasiswa::all();
        return view('tambahMahasiswa', compact('data'));

    }

    public function insertmahasiswa(Request $request){
        
        Mahasiswa::create($request ->all());
        return redirect()->route('mahasiswa');
    }

    public function tampilmahasiswa($NIM){
        
        $data = Mahasiswa::find($NIM);
        //dd($data);

        return view('tampilmahasiswa', compact('data'));
    }

    public function updatemahasiswa(Request $request, $NIM){
        
        $data = Mahasiswa::find($NIM);
        //dd($data);
        $data->update($request->all());
        return redirect()->route('mahasiswa')->with('success','Data Berhasil Di Ubah!');
    }

    public function deletemahasiswa($NIM){
        
        $data = Mahasiswa::find($NIM);
        //dd($data);
        $data->delete();
        return redirect()->route('mahasiswa')->with('success','Data Berhasil Di Hapus!');
    }
}
