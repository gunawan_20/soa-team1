<?php

namespace App\Http\Controllers;

use App\Models\Matakuliah;
use Illuminate\Http\Request;

class MatakuliahController extends Controller{
    public function index(Request $request){
        if($request->has('search')){
            $data = Matakuliah::where('nama_matkul', 'LIKE', '%' . $request->search.'%')->paginate(5);
        }
        else{
            $data = Matakuliah::paginate(5);
        }
        return view('matakuliah', compact('data'));
    }
    
    public function matakuliah_user(Request $request){
        if($request->has('search')){
            $data = Matakuliah::where('nama_matkul', 'LIKE', '%' . $request->search.'%')->paginate(5);
        }
        else{
            $data = Matakuliah::paginate(5);
        }
        return view('matakuliah_user', compact('data'));
    }

    public function tambahdata(){
        return view('tambahdata');
    }

    public function insertdata(Request $request){
        Matakuliah::create($request->all());
        return redirect()->route('matakuliah');
    }

    public function tampilkandata($id){
        $data = Matakuliah::find($id);
        return view('tampildata', compact('data'));
    }

    public function updatedata(Request $request, $id){
        $data = Matakuliah::find($id);
        $data->update($request->all());
        return redirect()->route('matakuliah');
    }
    
    public function deletedata($id){
        $data = Matakuliah::find($id);
        $data->delete();
        return redirect()->route('matakuliah');
    }
}
