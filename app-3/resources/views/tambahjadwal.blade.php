<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="public/css/style.css">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>CRUD jadwal kuliah</title>
  </head>
  <body>
   <h2 class="text-center mb-4">Tambah Data Jadwal</h2>
  
   <div class="container">
       <div class="row justify-content-center">
         <div class="col-6">
        <div class="card">
          <div class="card-body">
            <form action="/insertjadwal" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="mb-3">
                <label for="id_jadwal" class="form-label">Id Jadwal</label>
                <input type="integer" name="id_jadwal" class="form-control" id="exampleInputjadwal" placeholder="masukkan Id Jadwal">
              </div>
              <div class="mb-3">
                <label for="id_dosen" class="form-label">Id Dosen</label>
                <input type="integer" name="id_dosen" class="form-control" id="exampleInputdosen" placeholder="Masukkan Id Dosen">
              </div>
              <div class="mb-3">
                <label for="nama_mk" class="form-label">Matakuliah</label>
                <input type="text" name="nama_mk" class="form-control" id="exampleInputmk" placeholder="Masukkan Nama Matkul">
              </div>
              <div class="mb-3">
                <label for="hari" class="form-label">Hari</label>
                <select name="hari" class="form-select">
                  <option selected>choos Day</option>
                  <option value="1">Senin</option>
                  <option value="2">Selasa</option>
                  <option value="3">Rabu</option>
                  <option value="4">Kamis</option>
                </select>
              </div>
              <div class="mb-3">
                <label for="jam_awal" class="form-label">Jam Awal</label>
                <input type="time"name="jam_awal" class="form-control" id="exampleInputjawal" placeholder="Masukkan Jam Awal">
              </div>
              <div class="mb-3">
                <label for="jam_selesai" class="form-label">Jam Selesai</label>
                <input type="time" name="jam_selesai" class="form-control" id="exampleInputjselesai" placeholder="Masukkan Jam Selesai">
              </div>
              <div class="mb-3">
                <label for="id_gedung" class="form-label">Id Gedung</label>
                <input type="char" name="id_gedung" class="form-control" id="exampleInputdosen" placeholder="Masukkan Id Gedung">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="/jadwalmk" class="btn btn-dark ">Kembali </a>
            </form>
          </div>
        </div>
       </div>
       </div>
   </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    {{-- <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    --> --}}
  </body>
</html>