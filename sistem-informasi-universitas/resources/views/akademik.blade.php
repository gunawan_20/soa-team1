@extends('master')

@section('konten')

    <h2 class="text-center mb-4 mt-5">JADWAL MATAKULIAH</h2>
    
   <div class="container">
    <a href="/tambahjadwal" class="btn btn-success mt-3"> + Tambah </a>
    
    <div class="row g-3 align-items-center mt-3">
      <div class="col-auto">
        <form action="/akademik" method="GET">
        <input type="search" id="inputPassword6" name="search" class="form-control text-center" aria-describedby="passwordHelpInline" placeholder="Cari">
      </form>
      </div>
    </div>
       <div class="row">
        <table class="table mt-3">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Id Jadwal</th>
                <th scope="col">NIP Dosen</th>
                <th scope="col">Nama MK</th>
                <th scope="col">Hari</th>
                <th scope="col">Jam Awal</th>
                <th scope="col">Jam Selesai</th>
                <th scope="col">Id Gedung</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php
                $id=1;  
              @endphp
            @foreach ($data as $index => $datajadwal)
              <tr>
                <th scope="row">{{ $index+$data->firstItem() }}</th>
                <td>{{ $datajadwal->id_jadwal }}</td>
                <td>{{ $datajadwal->id_dosen }}</td>
                <td>{{ $datajadwal->nama_mk }}</td>
                <td>{{ $datajadwal->hari }}</td>
                <td>{{ $datajadwal->jam_awal }}</td>
                <td>{{ $datajadwal->jam_selesai}}</td>
                <td>{{ $datajadwal->id_gedung }}</td>
                <td>
                  <a href="/tampilkandata/{{ $datajadwal->id}}" class="btn btn-info">Edit</a>
                  <a href="#" class="btn btn-danger delete" data-id="{{ $datajadwal->id}}" data-nama="{{ $datajadwal->id_jadwal }}">Delete</a>  
                  </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          {{ $data->links() }}
       </div>
       <a href="/user" class="btn btn-dark mt-5">Kembali </a>
   </div>

   @section('sweetalert')
   <script>
     $('.delete').click(function(){
       var jadwalid = $(this).attr('data-id');
       var nama = $(this).attr('data-nama');
           
       swal({
       title: "Anda yakin ingin menghapusnya?",
       text: "Anda akan menghapus data dengan id  "+nama+" ",
       icon: "warning",
       buttons: true,
       dangerMode: true,
     })
     .then((willDelete) => {
         if (willDelete) {
           window.location = "/delete/"+jadwalid+""
           swal("Data berhasil dihapus!", {
             icon: "success",
           });
         } else {
           swal("Data batal dihapus");
         }
       });
     });
   </script>
 @endsection

@endsection
