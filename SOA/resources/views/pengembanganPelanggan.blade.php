@extends("master")

@section('title')
<title> PELANGGAN </title>
<script src="http://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
  <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
  <style>
    
    </style>
@endsection

@section('External_CSS')
<link rel="stylesheet" href="{{ asset('pelanggan.css') }}">
@endsection

@section("konten")

<!-- Jangan lupa bungkus dengan "container" -->
<div class="container mb-3">
    <h4 class="pt-5">Manajemen Pengembangan Pelanggan</h4>
    <p class="mb-4">Sumber Data : Sistem Informasi Sumber Daya</p>
    <div class="card">
        

        <div class ="mx-auto pt-5" id="container" style="width: 60%;">
            <p class="text-center"><b>Sebaran Jumlah Pelanggan</b></p>
            <p class="text-center">Per Bulan</p>
        
            <canvas id="canvas1"></canvas>
        </div>
        <div class = "mx-auto mt-4" id="container" style="width: 60%;">
            <p class="text-center"><b>Rekap Pesan Keluar</b></p>
            <p class="text-center">Per Bulan</p>
            <canvas id="canvas2"></canvas>
        </div>
       <script>
           const data = {
            labels: <?php echo json_encode($Months);?>,
            datasets: [{
                label: 'Jumlah Pelanggan',
                data: <?php echo json_encode($DataPelanggan);?>,
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: [
                    'rgba(123, 165, 254, 1)',
                    'rgba(77, 199, 181, 1)',
                    'rgba(240, 117, 126, 1)',
                    'rgba(245, 181, 81, 1)',
                    'rgba(66, 184, 245, 1)',
                    'rgba(123, 165, 254, 1)',
                    'rgba(77, 199, 181, 1)',
                    'rgba(240, 117, 126, 1)'
                ],
            }]
            };
            const config = {
            type: 'bar',
            data: data,
            options: {
                scales: {
                y: {
                    beginAtZero: true
                }
                }
            }
            };
       </script>
       <script>
            const pelangganChart = new Chart(
            document.getElementById('canvas1'),
            config
            );
       </script>

       <script>
           const dataPesan = {
            labels: <?php echo json_encode($Months);?>,
            datasets: [{
                label: 'Jumlah Pesan',
                data: <?php echo json_encode($DataPesan);?>,
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: [
                    'rgba(123, 165, 254, 1)',
                    'rgba(77, 199, 181, 1)',
                    'rgba(240, 117, 126, 1)',
                    'rgba(245, 181, 81, 1)',
                    'rgba(66, 184, 245, 1)',
                    'rgba(123, 165, 254, 1)',
                    'rgba(77, 199, 181, 1)',
                    'rgba(240, 117, 126, 1)'
                ],
            }]
            };
            const configPesan = {
            type: 'bar',
            data: dataPesan,
            options: {
                scales: {
                y: {
                    beginAtZero: true
                }
                }
            }
            };
       </script>
       <script>
            const pesanChart = new Chart(
            document.getElementById('canvas2'),
            configPesan
            );
       </script>

    </div>
</div>

@endsection