@extends('master')

@section('konten')
   <h2 class="text-center mb-4">Edit Data Jadwal</h2>
  
   <div class="container">
       <div class="row justify-content-center">
         <div class="col-6">
        <div class="card">
          <div class="card-body">
            <form action="/updatedata/{{$data->id}}" method="POST" enctype="multipart/form-data">
              @csrf
              <div class="mb-3">
                <label for="id_jadwal" class="form-label">Id Jadwal</label>
                <input type="integer" name="id_jadwal" class="form-control" id="exampleInputjadwal" placeholder="masukkan Id Jadwal" value="{{ $data ->id_jadwal }}">
              </div>
              <div class="mb-3">
                <label for="id_dosen" class="form-label">Id Dosen</label>
                <input type="integer" name="id_dosen" class="form-control" id="exampleInputdosen" placeholder="Masukkan Id Dosen" value="{{ $data ->id_dosen }}">
              </div>
              <div class="mb-3">
                <label for="nama_mk" class="form-label">Matakuliah</label>
                <input type="text" name="nama_mk" class="form-control" id="exampleInputmk" placeholder="Masukkan Nama Matkul" value="{{ $data ->nama_mk}}">
              </div>
              <div class="mb-3">
                <label for="hari" class="form-label">Hari</label>
                <select name="hari" class="form-select">
                  <option selected>{{ $data->hari }}</option>
                  <option value="1">Senin</option>
                  <option value="2">Selasa</option>
                  <option value="3">Rabu</option>
                  <option value="4">Kamis</option>
                </select>
              </div>
              <div class="mb-3">
                <label for="jam_awal" class="form-label">Jam Awal</label>
                <input type="time"name="jam_awal" class="form-control" id="exampleInputjawal" placeholder="Masukkan Jam Awal" value="{{ $data ->jam_awal }}">
              </div>
              <div class="mb-3">
                <label for="jam_selesai" class="form-label">Jam Selesai</label>
                <input type="time" name="jam_selesai" class="form-control" id="exampleInputjselesai" placeholder="Masukkan Jam Selesai" value="{{ $data ->jam_selesai}}">
              </div>
              <div class="mb-3">
                <label for="id_gedung" class="form-label">Id Gedung</label>
                <input type="char" name="id_gedung" class="form-control" id="exampleInputdosen" placeholder="Masukkan Id Gedung" value="{{ $data ->id_gedung}}">
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
              <a href="/akademik" class="btn btn-dark ">Kembali </a>
            </form>
          </div>
        </div>
       </div>
       </div>
   </div>

@endsection
