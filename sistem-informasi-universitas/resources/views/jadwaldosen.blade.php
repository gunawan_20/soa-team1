@extends('master')

@section('konten')

<h3>JADWAL PERKULIAHAN DOSEN</h3>
<div class="card p-3 mt-4 mb-2 d-inline search1" >
  <input type="search" class=" search" name="search" id="search" style="width:300px">
    <button type="button" class="btn btn-primary  p-1" style="width:60px">
        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
        </svg>
    </button>
</div>
<div class="card mt-3" style="width: 98%;">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">NIP Dosen</th>
            <th scope="col">Nama Dosen</th>
            <th scope="col">Mata Kuliah</th>
            <th scope="col">Hari</th>
            <th scope="col">Jam Awal</th>
            <th scope="col">Jam Selesai</th>
            <th scope="col">Gedung</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($data as $index => $baris)
    
            <tr>
                <th scope="row">{{ $index+$data->firstItem() }}</th>
                <td>{{ $baris->id_dosen }}</td>
                <td>{{ $baris->nama_dosen }}</td>
                <td>{{ $baris->nama_mk }}</td>
                <td>{{ $baris->hari }}</td>
                <td>{{ $baris->jam_awal }}</td>
                <td>{{ $baris->jam_selesai}}</td>
                <td>{{ $baris->id_gedung }}</td>
            </tr>
                
            @endforeach
        </tbody>
        {{ $data->links() }}
      </table>
  </div>


    
@endsection