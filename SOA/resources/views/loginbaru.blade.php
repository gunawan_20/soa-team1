@extends("master")

@section('title')
<title> LOGIN | ERP DATAHOUSE </title>
@endsection

@section('External_CSS')
<link rel="stylesheet" href="{{ asset('styleloginbaru.css') }}">
@endsection

@section("konten")

<!-- Jangan lupa bungkus dengan "container" -->
<img src="{{ asset('images/Data extraction-amico 1.png') }}" class="gambar" alt="" srcset="" width="500px" height="400px">

<div class="card-body kotaklogin">
    <h2 class="d-flex justify-content-center tekslogin">LOGIN</h2>
    <form method="POST" action="/login/kirim">
        @csrf
          <div class="mb-3">
            <label for="email" class="form-label">Email</label>
            <input type="email" class="form-control tombol" name="email" id="email" placeholder="mail@unnes.ac.id">
          </div>
          <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control tombol" name="password"id="password" placeholder="min 8 character">
          </div>
          <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Remember me</label>
            <a href="#">Forgot password?</a>
          </div>
          <button type="submit" class="btn btn-primary tombol1" style="background-color: blue">Masuk</button>
        </form>
        <hr class="garis1"><p class="d-flex justify-content-center">atau</p><hr class="garis2">
        <a href="{{ url('auth/google') }}"><button type="button" class="btn btn-danger tombol tombol1">
            <img src="{{ asset('images/icon_google.png') }}" width="20px" height="20px">
              Sign in with Google
          </button>
          </a>

  </div>

@endsection