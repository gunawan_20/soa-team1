@extends('master')
    
@section('konten')
<h2 class="mb-3 d-flex justify-content-center">Edit Data Nilai</h2>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-7">
          <div class="card">
              <div class="card-body">
                  <form action="/editNilai/{{ $data->id }}" method="POST" enctype="multipart/form-data">
                      @csrf
                      <div class="mb-3">
                        <label for="nilai" class="form-label">NILAI</label>
                        <input type="number" class="form-control" name ="nilai" id="nilai" value="{{ $data->nilai }}">
                      </div>
                      <div class="mb-3">
                          <label for="komentar" class="form-label">KOMENTAR</label>
                          <input type="text" name="komentar" class="form-control"  id="komentar" value="{{ $data->komentar }}">
                      </div>
                      <button type="submit" class="btn btn-primary login">Edit</button>
                    </form>
                    <a href="/nilai" id="kembali"><button type="submit" class="btn btn-danger mt-2">Kembali</button></a>
               </div>
            </div>
        </div>
    </div>
</div>
@endsection