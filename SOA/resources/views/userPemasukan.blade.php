@extends("masterdashboard")

@section('title')
<title> PEMASUKAN </title>
@endsection

@section('External_CSS_JS')

@endsection

@section("judul_laman")
@endsection

@section("konten")

<!-- Jangan lupa bungkus dengan "container" -->
<div class="container mx-auto mb-4" style="width: 90%;">
    <h4 class="pt-2">TABEL PEMASUKAN</h4>
        <p class="mb-4">Sumber Data : Sistem Informasi Penjualan</p>
    <div class="card"> 
        <div class="row">
            <div class="column">
                <div class ="mx-auto pt-5 pb-5" id="container" style="width: 90%;">
                    <div class="container cardBoxHasil">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Data Pemasukan</th>
                                    <th scope="col">Jumlah</th>
                                </tr>
                                </thead>
                                <tbody class="table-group-divider border-primary">
                                <tr>
                                    <th scope="row">1</th>
                                    <td><p>Barang Terjual</p></td>
                                    <td>{{ $data['Jumlah_barang_terjual'] }}</td>
                                </tr>

                                <tr>
                                    <th scope="row">2</th>
                                    <td><p>Transaksi Penjualan</p></td>
                                    <td>{{ $data['Jumlah_transaksi_penjualan'] }}</td>
                                </tr>

                                <tr>
                                    <th scope="row">3</th>
                                    <td><p>Transaksi Pembelian</p></td>
                                    <td>{{ $data['Jumlah_transaksi_pembelian'] }}</td>
                                </tr>

                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
@endsection