<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Jurusan</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
</head>
<body>
    <div class="container mt-5">
        @if (Session::get('sukses'))
        <div class="alert alert-success text-center">
            {{ Session::get('sukses') }}
        </div>
        @endif

        @if (Session::get('gagal'))
        <div class="alert alert-danger text-center">
            {{ Session::get('gagal') }}
        </div>
        @endif
    </div>

    <div class="container mt-5">
        <div class="text-center mb-5">
            <h3>Daftar Gedung</h3>
        </div>

        <div class="text-center mb-5">
            <a href="{{ route('tambah-gedung') }}" class="btn btn-success">Tambah Gedung</a>
        </div>

        <table id="tb_gedung" class="table table-sm" style="width:100%">
            <thead>
                <tr>
                    <th>ID Gedung</th>
                    <th>Nama Jurusan</th>
                    <th>Nama Gedung</th>
                    <th>Kode Ruangan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($gedung as $g)
                <tr class="align-middle">
                    <td>{{ $g->id_gedung }}</td>
                    <td>{{ $g->nama_jurusan }}</td>
                    <td>{{ $g->nama_gedung }}</td>
                    <td>{{ $g->kd_ruangan }}</td>
                    <td>
                        <div class="d-flex justify-content-between">
                            <a href="{{ route('edit-gedung',$g->id_gedung) }}" class="btn btn-primary">Edit</a>
                            <a href="{{ route('hapus-gedung',$g->id_gedung) }}" class="btn btn-danger">Hapus</a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#tb_gedung').DataTable();
        } );
    </script>
</body>
</html>
