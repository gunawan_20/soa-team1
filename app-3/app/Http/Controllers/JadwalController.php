<?php

namespace App\Http\Controllers;

use App\Models\jadwal;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function index(Request $request){
        if($request->has('search')){
            $data = jadwal::where('nama_mk', 'LIKE', '%'.$request->search.'%')->paginate(2);
        }else{
            $data = jadwal::paginate(2);
        }
        return view('jadwalmk',compact('data'));
    }

     public function tambahjadwal(){
       return view('tambahjadwal'); 
    }

    public function insertjadwal(Request $request){
        jadwal::create($request->all());
        return redirect()->route('jadwalmk')->with('success', 'Data Berhasil Ditambahkan');
    }

    public function tampilkandata($id){
        $data = jadwal::find($id);
        //dd($data);
        return view('tampildata',compact('data'));
     }

     public function updatedata(Request $request, $id){
        $data = jadwal::find($id);
        $data -> update($request->all());
        return redirect()->route('jadwalmk')->with('success', 'Data Berhasil Di Update');
     }

     public function delete($id){
        $data = jadwal::find($id);
        $data->delete();
        return redirect()->route('jadwalmk')->with('success', 'Data Berhasil Di Delete');
     }

}
