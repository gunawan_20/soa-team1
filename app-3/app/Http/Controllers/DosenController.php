<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Dosen;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tabelDosen(Request $request)
    {
        $data = Dosen::all();
        return view('tabelDosen')->with ([
            'data' => $data
        ]);
    }       

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createDosen()
    {
        return view("createDosen");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDosen(Request $request)
    {
        $data = $request->except(['_token']);
        Dosen::insert($data);
        return redirect("Dosen");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showDosen($id)
    {
        $data = Dosen::FindOrFail($id);
        return view('showDosen')->with ([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editDosen($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDosen(Request $request, $id)
    {
        $item = Dosen::findOrFail($id);
        $data = $request->except(['_token']);
        $item->update($data);
        return redirect('Dosen');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDosen($id)
    {
        $data = Dosen::findOrFail($id);
        $data->delete();
        return redirect('Dosen');
    }
}
