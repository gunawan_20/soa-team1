<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades;

class PenjualanController extends Controller
{
    public function penjualan() 
    {
        $client = new Client();
        $request = $client->get('http://kelompok6.live/api/sales');
        $response = $request -> getBody()->getContents();
        $data = json_decode($response, true);
        
        $month = array('Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $terjual = $data['Jumlah_barang_terjual'];
        $terjualperbulan = array(0,0,0,0,0,$data['Jumlah_barang_terjual_perbulan'],0,0,0,0,0,0 );
        $transaksi = $data['Jumlah_transaksi_penjualan'];
        $transaksiperbulan = array(0,0,0,0,0,$data['Jumlah_transaksi_penjualan_perbulan'],0,0,0,0,0,0 );
        return view('penjualan',['Months' => $month,'Jumlah_barang_terjual'=> $terjual,'Jumlah_barang_terjual_perbulan'=>$terjualperbulan, 'Jumlah_transaksi_penjualan'=>$transaksi,
        'Jumlah_transaksi_penjualan_perbulan'=> $transaksiperbulan]);
    }

}



