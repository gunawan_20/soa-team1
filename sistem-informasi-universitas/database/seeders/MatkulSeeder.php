<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MatkulSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('matkuls')->insert([
            'id_matkul' => '10',
            'nama_matkul' => 'Jaringan Komputer',
            'sks' => '3',
        ]);
    }
}
