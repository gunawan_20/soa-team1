<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class PortalController extends Controller{
    public function portal(){
       // pengguna aktif
       $pengguna = Http::get('http://20.227.138.204/api/user/active/count');
       $datapengguna = json_decode($pengguna,true);
       $jumlahpengguna = $datapengguna['active user'];

        // pesan keluar
       $pesan = Http::get('http://www.waniaebro.xyz/api/soa');
       $datapesan = json_decode($pesan,true);
       $jumlahwa = $datapesan['jumlah_whatsapp_keluar'];
       $jumlahemail = $datapesan['jumlah_email_keluar'];
       $jumlahpesan = $jumlahwa + $jumlahemail; 

        // jumlah transaksi
        $transaksi = Http::get('http://kelompok6.live/api/sales');
        $datatransaksi = json_decode($transaksi,true);

        $jumlahtransaksipenjualan = $datatransaksi['Jumlah_transaksi_penjualan' ];
        $jumlahtransaksipembelian = $datatransaksi['Jumlah_transaksi_pembelian' ];
        $jumlahtransaksi = $jumlahtransaksipenjualan + $jumlahtransaksipembelian; 
        
        // jumlah karyawan
        $karyawan = Http::withToken('1|dp0UWOPpm8yRAD8Nd58ELuYPxLUTIdEjCfHTdh3j')->get('http://kelompok4.tech/api/v1/pegawai');
        $datakaryawan = json_decode($karyawan,true);

        $jumlahkaryawan = count($datakaryawan['data']);
        // dd($jumlahkaryawan);

        //Pegawai
        $pegawai = Http::withToken('2|P8V8iLUsGE9xVxItfgDQCGABQQUPcwmjMQ3VJ6r6')->get('http://kelompok4.tech/api/v1/pegawai');
        $dataPegawai = json_decode($pegawai,true);
        $jenisKelamin =[];
        for ($i=0; $i < 11; $i++) { 
            $jenisKelamin[$i] = $dataPegawai['data'][$i]['gender'];
        }
        $jumlahsetiapjeniskelamin = array_count_values($jenisKelamin);
        $jumlahsetiapjeniskelaminFix = array_values($jumlahsetiapjeniskelamin);
        $jenisKelaminPegawai = array_unique($jenisKelamin);
        $jenisKelaminPegawaiFix = array_values($jenisKelaminPegawai);

        //Penjualan
        $penjualan = Http::get('http://kelompok6.live/api/sales');
        $data = json_decode($penjualan, true);
        $month = array('Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $terjualperbulan = array(0,0,0,0,0,$data['Jumlah_barang_terjual_perbulan'],0,0,0,0,0,0 );

        return view('portaldata',[
            'jumlahkaryawan'=>$jumlahkaryawan,
            'jumlahtransaksi'=>$jumlahtransaksi,
            'jumlahpesan'=>$jumlahpesan,
            'jumlahpengguna'=>$jumlahpengguna,
            'JenisKelamin'=>$jenisKelaminPegawaiFix,
           'JumlahSetiapJenisKelamin'=>$jumlahsetiapjeniskelaminFix,
           'Jumlah_barang_terjual_perbulan'=>$terjualperbulan,
           'Months' => $month

         ]);
    }
}
