<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset ('style.css') }}">
    <title>CRUD | MAHASISWA</title>
  </head>
  <body>
      <h1 class="text-center mb-4 mt-4"><b> DATA MAHASISWA</b></h1>
    <div class="container">
        <a href="/tambahmahasiswa"><button type="button" class="btn btn-primary">+ Tambah</button></a>
        <a href="/logoutmahasiswa"><button type="button" class="btn btn-danger buton1"> LOG OUT</button></a>
        
        <table class="table  table-striped ">
            <thead>
              <tr>

                <div class="input mb-3 mt-3">
                  <form action="/" method="GET">
                  <input type="search" id ="search" name ="search"  class="form-control2" placeholder="  Cari"  aria-describedby="button-addon2">
                  
                  </form>
                </div>

                <th scope="col">#</th>
                <th scope="col">NIM</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">Alamat</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @php    
              $no=1;
              @endphp
              @foreach ($data as $index => $dt)
              
              <tr>
                <th scope="row">{{ $index + $data->firstitem() }}</th>
                <td>{{ $dt->NIM }}</td>
                <td>{{ $dt->nama }}</td>
                <td>{{ $dt->email }}</td>
                <td>{{ $dt->alamat }}</td>
                <td>
                    <a href="/deletedatamahasiswa/{{ $dt->id }}" class="btn btn-danger delete">Hapus</a>
                    <a href="/tampilkandatamahasiswa/{{ $dt->id }}"class="btn btn-warning edit">Edit</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $data->links() }}
      </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>