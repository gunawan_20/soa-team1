@extends("master")

@section('title')
<title> Manajemen Kepegawaian </title>
<script src="http://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
  <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
@endsection

@section('External_CSS')

@endsection

@section("konten")

<!-- Jangan lupa bungkus dengan "container" -->
<div class="container mb-3">
    <h4 class="pt-5">Manajemen Kepegawaian</h4>
    <p class="mb-4">Sumber Data : Sistem Informasi Kepegawaian</p>
    <div class="card">
        
        <div class="row">
            <div class="col">
                <div class ="mx-auto pt-5" id="container" style="width: 60%;">
                    <p class="text-center">Berdasarkan Jenjang Pendidikan</p>
                        <canvas id="canvas1"></canvas>
                      
                </div>
            </div>
            <div class="col">
                <div class = "mx-auto pt-5" id="container" style="width: 60%;">
                    <p class="text-center">Berdasarkan Jenis Kelamin</p>
                    <canvas id="canvas2"></canvas>
                </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
                <div class ="mx-auto pt-5 pb-5" id="container" style="width: 60%;">
                    <p class="text-center">Berdasarkan Umur</p>
                    <canvas id="canvas3"></canvas>
                </div>
            </div>
            <div class="col">
                <div class = "mx-auto pt-5 pb-5" id="container" style="width: 60%;">
                    <p class="text-center">Berdasarkan Jabatan Fungsional</p>
                    <canvas id="canvas4"></canvas>
                </div>
            </div>
          </div>
        
        
    <script>
            
  const Pendidikan = <?php echo json_encode($PendidikanPegawai);?>;

  const data = {
    labels: Pendidikan,
    datasets: [{
      label: 'My First dataset',
      backgroundColor: [
            'rgba(123, 165, 254, 1)',
            'rgba(77, 199, 181, 1)',
            'rgba(240, 117, 126, 1)'
            ],
      data: <?php echo json_encode($JumlahSetiapPendidikan);?>,
    }]
  };

  const config = {
    type: 'pie',
    data: data,
    options: {}
  };
    </script>
<script>
    const pendidikanChart = new Chart(
      document.getElementById('canvas1'),
      config
    );
  </script>

<script>
            
    const jenisKelamin = <?php echo json_encode($JenisKelamin)?>;
  
    const dataJenisKelamin = {
      labels: jenisKelamin,
      datasets: [{
        label: 'My First dataset',
        backgroundColor: [
              'rgba(123, 165, 254, 1)',
              'rgba(77, 199, 181, 1)'
              ],
        data: <?php echo json_encode($JumlahSetiapJenisKelamin);?>,
      }]
    };
  
    const configJenisKelamin = {
      type: 'pie',
      data: dataJenisKelamin,
      options: {}
    };
      </script>
  <script>
      const jenisKelaminChart = new Chart(
        document.getElementById('canvas2'),
        configJenisKelamin
      );
    </script>

<script>
            
    const Umur = <?php echo json_encode($Umur);?>;
  
    const dataUmur = {
      labels: Umur,
      datasets: [{
        label: 'Jumlah',
        backgroundColor: [
              'rgba(123, 165, 254, 1)',
              'rgba(77, 199, 181, 1)',
              'rgba(240, 117, 126, 1)'
              ],
        data: <?php echo json_encode($JumlahUmur);?>,
      }]
    };
  
    const configUmur = {
      type: 'bar',
      data: dataUmur,
      options: {}
    };
      </script>
  <script>
      const myChart = new Chart(
        document.getElementById('canvas3'),
        configUmur
      );
    </script>

<script>
            
    const Jabatan = <?php echo json_encode($JabatanPerusahaan)?>;
  
    const dataJabatan = {
      labels: Jabatan,
      datasets: [{
        label: 'My First dataset',
        backgroundColor: [
              'rgba(123, 165, 254, 1)',
              'rgba(77, 199, 181, 1)',
              'rgba(240, 117, 126, 1)',
              'rgba(123, 165, 25, 1)',
              'rgba(77, 199, 198, 1)',
              'rgba(240, 117, 176, 1)',
              'rgba(123, 165, 250, 1)',
              'rgba(77, 199, 151, 1)',
              'rgba(240, 117, 196, 1)',
              'rgba(123, 165, 21, 1)',
              'rgba(77, 199, 109, 1)',
              'rgba(240, 117, 146, 1)',
              'rgba(123, 165, 5, 1)',
              'rgba(77, 199, 19, 1)',
              'rgba(240, 117, 246, 1)'
              ],
        data: <?php echo json_encode($JumlahSetiapJabatan)?>,
      }]
    };
  
    const configJabatan = {
      type: 'bar',
      data: dataJabatan,
      options: {}
    };
      </script>
  <script>
      const jabatanChart = new Chart(
        document.getElementById('canvas4'),
        configJabatan
      );
    </script>

        

    </div>
</div>
@endsection