<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Header;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\Pegawai;


class DataApiController extends Controller
{
    public function index()
    {
        
        
        $client = new Client();
        $request = $client->get('http://www.waniaebro.xyz/api/soa');
        $response = $request->getBody()->getContents();
        $data = json_decode($response,true);
        
        $month = array('Januari','Febuari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $dataPelanggan  = array(0,0,0,0,0,$data['jumlah_pesanan_diterima'],0,0,0,0,0,0 );
        $dataPesan = array(0,0,0,0,0,$data['jumlah_whatsapp_keluar'],0,0,0,0,0,0 );
        return view('pengembanganPelanggan',['Months' => $month, 'DataPelanggan' => $dataPelanggan, 'DataPesan'=>$dataPesan]);
    }
    public function pegawai ()
    {
        // umur dan jenis kelamin

        $pegawai = Http::withToken('2|P8V8iLUsGE9xVxItfgDQCGABQQUPcwmjMQ3VJ6r6')->get('http://kelompok4.tech/api/v1/pegawai');
        $dataPegawai = json_decode($pegawai,true);
        //dd($dataPegawai);
        $jumlahPegawai = count($dataPegawai['data']);
        $umur = [];
        for ($i=0; $i < 11; $i++) { 
            $umur[$i]= $dataPegawai['data'][$i]['umur'];
        }
        $jenisKelamin =[];
        for ($i=0; $i < 11; $i++) { 
            $jenisKelamin[$i] = $dataPegawai['data'][$i]['gender'];
        }
        //dd($umur);
        $jumlahSetiapUmur = array_count_values($umur);
        $umurPegawai = array_unique($umur);
        $jumlahSetiapUmurFix = array_values($jumlahSetiapUmur);
        $umurPegawaiFix = array_values($umurPegawai);

        //jeniskelamin
        //dd($jenisKelamin);

        $jumlahsetiapjeniskelamin = array_count_values($jenisKelamin);
        $jumlahsetiapjeniskelaminFix = array_values($jumlahsetiapjeniskelamin);
        $jenisKelaminPegawai = array_unique($jenisKelamin);
        $jenisKelaminPegawaiFix = array_values($jenisKelaminPegawai);

        //pendidikan
        $pendidikan = Http::withToken('4|dxd46FFOcCo9supnmqa7dgv1TTnFwBpUzK8yc6Yh')->get('http://kelompok4.tech/api/v1/pendidikanpegawai');
        $dataPendidikan = json_decode($pendidikan,true);
        
        $jumlahPendidikan = count($dataPendidikan['data']);
        $pendidikan = [];

        for ($i=0; $i < $jumlahPendidikan; $i++) { 
            $pendidikan[$i]= $dataPendidikan['data'][$i]['jenjang'];
        }
        $jumlahSetiapPendidikan = array_count_values($pendidikan);
        $pendidikanPegawai = array_unique($pendidikan);

        $pendidikanPegawaiFix = array_values($pendidikanPegawai);
        $jumlahSetiapPendidikanFix = array_values($jumlahSetiapPendidikan);
       // dd($jumlahSetiapPendidikanFix);


       //jabatan
       $jabatan = Http::withToken('3|LZvrOHaDIqoDIbKSLXfVIUThyFQRqCnsLTRLyL2k')->get('http://kelompok4.tech/api/v1/jabatan');
        $dataJabatan = json_decode($jabatan,true);

        
        $jumlahJabatan = count($dataJabatan['data']);
        $jabatanPerusahaan = [];
        for ($i=0; $i < $jumlahJabatan; $i++) { 
            $jabatanPerusahaan[$i]= $dataJabatan['data'][$i]['name'];
        }
        $jumlahSetiapJabatan = array_count_values($jabatanPerusahaan);
        $jumlahSetiapJabatanFix = array_values($jumlahSetiapJabatan);
        $jabatanperusahaan1 = array_unique($jabatanPerusahaan);
        $jabatanPerusahaanFix = array_values($jabatanperusahaan1);
        

       return view('pegawai',[
           'Umur'=>$umurPegawaiFix,
           'JumlahUmur'=>$jumlahSetiapUmurFix, 
           'JenisKelamin'=>$jenisKelaminPegawaiFix,
           'JumlahSetiapJenisKelamin'=>$jumlahsetiapjeniskelaminFix, 
           'PendidikanPegawai'=>$pendidikanPegawaiFix, 
           'JumlahSetiapPendidikan'=>$jumlahSetiapPendidikanFix,
           'JabatanPerusahaan'=>$jabatanPerusahaanFix,
           'JumlahSetiapJabatan'=>$jumlahSetiapJabatanFix
        ]);

    }

    public function pegawaiInternal()
    {
        $pegawai = Http::withToken('2|P8V8iLUsGE9xVxItfgDQCGABQQUPcwmjMQ3VJ6r6')->get('http://kelompok4.tech/api/v1/pegawai');
        $dataPegawai = json_decode($pegawai,true);
        //dd($dataPegawai['data'][0]['name']);
        $jumlahPegawai = count($dataPegawai['data']);
        $nama = [];
        $email =[];
        $alamat =[];
        $no_telp = [];
        $umur = [];
        $gender = [];
        for ($i=0; $i < $jumlahPegawai; $i++) { 
            $nama[$i]= $dataPegawai['data'][$i]['name'];
            $email[$i]= $dataPegawai['data'][$i]['email'];
            $alamat[$i]= $dataPegawai['data'][$i]['alamat'];
            $no_telp[$i]= $dataPegawai['data'][$i]['no_telp'];
            $umur[$i]= $dataPegawai['data'][$i]['umur'];
            $gender[$i]= $dataPegawai['data'][$i]['gender'];
        }
        

        return view('dataPegawai',[
            'nama' =>$nama,
            'email' =>$email,
            'alamat' =>$alamat,
            'no_telp'=>$no_telp,
            'umur'=>$umur,
            'gender'=>$gender,
            'jumlah' =>$jumlahPegawai
        ]);
    }


    public function insertToAPi ()
    {
        $pegawai = Http::withToken('4|UW7iP8O46dElSyWwA9bAa4ePxRWzfMyRwEAW88oe')->get('http://kelompok4.tech/api/v1/pegawai');
        $dataPegawai = json_decode($pegawai,true);

        Pegawai::create([
            'nama'=>$dataPegawai['data'][0]['name'],
            'jabatan_id'=>$dataPegawai['data'][0]['jabatan_id']
        ]);
        
    }
    


}
