@extends('master')
    
@section('konten')
  <h2 class="mb-3 d-flex justify-content-center">Tambah Data Nilai</h2>
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-7">
              <div class="card">
                  <div class="card-body">
                        <form action="/insertDataNilai" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="mb-3">
                                <label for="NIM" class="form-label">PILIH NIM MAHASISWA</label>
                                <select class="form-select" name ="NIM" id="NIM">
                                    <option selected>Pilih NIM</option>
                                    @foreach ($datanim as $datamhs)
                                        <option value="{{ $datamhs->NIM }}">{{ $datamhs->NIM }}</option>    
                                    
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="id_matkul" class="form-label">PILIH ID MATAKULIAH</label>
                                <select class="form-select" name ="id_matkul" id="id_matkul">
                                    <option selected>Pilih ID Matkul</option>
                                    @foreach ($datamk as $datamk)
                                        <option value="{{ $datamk->id_matkul }}">{{ $datamk->id_matkul }}</option>    
                                    @endforeach
                                </select>
                            </div>
       
                            <div class="mb-3">
                              <label for="nilai" class="form-label">NILAI</label>
                              <input type="number" class="form-control" name ="nilai" id="nilai">
                            </div>
                            <div class="mb-3">
                                <label for="komentar" class="form-label">KOMENTAR</label>
                                <input type="text" name="komentar" class="form-control"  id="komentar" >
                            </div>
                            <button type="submit" class="btn btn-primary login">Tambah</button>
                          </form>
                          <a href="/nilai" id="kembali"><button type="submit" class="btn btn-danger mt-2">Kembali</button></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
@endsection