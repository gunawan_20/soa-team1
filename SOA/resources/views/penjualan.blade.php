@extends("master")

@section('title')
<title> Data Penjualan </title>
{{-- <script src="http://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
  <script src="http://www.chartjs.org/samples/latest/utils.js"></script> --}}
@endsection

@section('External_CSS')
<link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.2" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('portaldata.css') }}">
<link rel="stylesheet" href="{{ asset('penjualan.css') }}">
@endsection

@section("konten")
<script src="http://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
<script src="http://www.chartjs.org/samples/latest/utils.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="scroll">
    <div class="container mb-5">
        <h4 class="pt-5">Manajemen Penjualan</h4>
        <p class="mb-4">Sumber Data : Sistem Informasi Penjualan</p>
        <div class="card">
            <div class="row">
                <div class="col">
                    <div class ="mx-auto pt-5" id="container" style="width: 80%;">
                        <div>
                            <p class="text-center"><b>Total Barang Terjual</b></p>
                            <p class="text-center">Berdasarkan data transaksi penjualan</p>
                                <div class="container cardBoxHasil">
                                    <div class="d-flex justify-content-between">
                                    <div class="hasil">{{ $Jumlah_barang_terjual }}</div>
                                    <p class="ket">barang</p>
                                    </div>
                                </div>
                        </div>   
                    </div>
                </div>
                <div class="col">
                    <div class = "mx-auto pt-5" id="container" style="width: 80%;">
                        <p class="text-center"><b>Banyaknya Barang Terjual</b></p>
                        <p class="text-center">Per Bulan</p>
                        <canvas id="canvas2"></canvas>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class ="mx-auto pt-5 pb-5" id="container" style="width: 80%;">
                        <div>
                            <p class="text-center"><b>Total Transaksi Penjualan</b></p>
                            <p class="text-center">Berdasarkan akumulasi penjualan</p>
                                <div class="container cardBoxHasil">
                                    <div class="d-flex justify-content-between">
                                    <div class="hasil">{{ $Jumlah_transaksi_penjualan }}</div>
                                    <p class="ket">transaksi</p>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class = "mx-auto pt-5 pb-5" id="container" style="width: 80%;">
                        <p class="text-center"><b>Jumlah Transaksi</b></p>
                        <p class="text-center">Per Bulan</p>
                        <canvas id="canvas4"></canvas>
                    </div>
                </div>
            </div>
            <script>
                const PenjualanBarang = <?php echo json_encode($Jumlah_barang_terjual);?>;
                const dataPenjualanBarang = {
                labels: 'PenjualanBarang'
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: [
                        'rgba(123, 165, 254, 1)',
                        'rgba(77, 199, 181, 1)',
                        'rgba(240, 117, 126, 1)',
                        'rgba(245, 181, 81, 1)',
                        'rgba(66, 184, 245, 1)',
                        'rgba(123, 165, 254, 1)',
                        'rgba(77, 199, 181, 1)',
                        'rgba(240, 117, 126, 1)'
                    ],
                    data: <?php echo json_encode($Jumlah_barang_terjual);?>,
                }]
                };
                const configPenjualanBarang = {
                    type: 'pie',
                    data: dataPenjualanBarang,
                    options: {}
                };
                
            </script> 
            <script>
                const PenjualanBarangChart = new Chart(
                document.getElementById('canvas1'),
                configPenjualanBarang
            );
            </script>
            <script>
                const data = {
                labels: <?php echo json_encode($Months);?>,
                datasets: [{
                    label: 'Jumlah Barang Terjual',
                    data: <?php echo json_encode($Jumlah_barang_terjual_perbulan);?>,
                    borderColor: 'rgb(255, 99, 132)',
                    backgroundColor: [
                        'rgba(240, 117, 126, 1)',
                        'rgba(245, 181, 81, 1)',
                        'rgba(66, 184, 245, 1)',
                        'rgba(123, 165, 254, 1)',
                        'rgba(77, 199, 181, 1)',
                        'rgba(240, 117, 126, 1)'
                    ],
                }]
                };
                const config = {
                type: 'bar',
                data: data,
                options: {
                    scales: {
                    y: {
                        beginAtZero: true
                    }
                    }
                }
                };
            </script>    
            <script>
                const penjualanChart = new Chart(
                document.getElementById('canvas2'),
                config
                );
            </script>

            <script>
                const dataTransaksi = {
                labels: <?php echo json_encode($Months);?>,
                datasets: [{
                    label: 'Jumlah Transaksi',
                    data: <?php echo json_encode($Jumlah_transaksi_penjualan_perbulan);?>,
                    borderColor: 'rgb(255, 99, 132)',
                    backgroundColor: [
                        'rgba(77, 199, 181, 1)',
                        'rgba(240, 117, 126, 1)',
                        'rgba(245, 181, 81, 1)',
                        'rgba(66, 184, 245, 1)',
                        'rgba(123, 165, 254, 1)',
                        'rgba(77, 199, 181, 1)',
                        'rgba(240, 117, 126, 1)'
                    ],
                }]
                };
                const configTransaksi = {
                type: 'bar',
                data: dataTransaksi,
                options: {
                    scales: {
                    y: {
                        beginAtZero: true
                    }
                    }
                }
                };
            </script>    
            <script>
                const TransaksiChart = new Chart(
                document.getElementById('canvas4'),
                configTransaksi
                );
            </script>
        </div>
    </div>
</div>
@endsection