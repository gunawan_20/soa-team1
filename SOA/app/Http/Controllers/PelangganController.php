<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PelangganController extends Controller{
    public function pelanggan(){
        // data pelanggan 
       $pelanggan = Http::get('http://www.waniaebro.xyz/api/soa');
       $datapelanggan = json_decode($pelanggan,true);
       
       return view('dataPelanggan', compact('datapelanggan'));
    }
}
