<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;



class BarangController extends Controller
{
    public function getData() 
    {
        $client = new Client();
        $request = $client->get('https://marvelaw.com/api/kategoris');
        $request = $client->get('https://marvelaw.com/api/barangs');
        $response = $request -> getBody()->getContents();
        $data = json_decode($response, true);
        
        $kategori = array('AC','Monitor','Keyboard','Mouse','Speaker','MousePad','Laptop', 
        'Motherboat','RAM','CPU','VGA','SSD','HDD','Power Supplay','Handphone');
        $stock = array(15,10,180,747,0,0,20,0,56,295,86,0,0,0,247);
        $kategori1 = array('Mouse','Speaker','MousePad','Motherboat','SSD','HDD','Power Supplay');
        $bk = array(747,0,0,0,0,0,0);
        $harga = array([3000000], [3250000],[1380000, 300000000], [500000],[0],[0],[15500000],[0],[550000],
        [1560000,5260000],[5750000],[0],[0],[0],[100000,2449000]);
        return view('barang',['Kategori'=>$kategori,'Stock'=>$stock, 'Kategori1'=>$kategori1, 'bk'=> $bk,
        'harga'=> $harga]);
    }
}
