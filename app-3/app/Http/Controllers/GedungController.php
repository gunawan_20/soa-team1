<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\gedungs;
use App\Models\jurusans;
use Illuminate\Support\Facades\DB;

class gedung extends Controller
{
    function index(){
        $data = array(
            "gedung" => DB::table('gedungs')->select('gedungs.id_gedung as id_gedung', 'jurusans.nama_jurusan as nama_jurusan', 'gedungs.nama_gedung as nama_gedung', 'gedungs.kd_ruangan as kd_ruangan')->join('jurusans', 'gedungs.id_jurusan','=','jurusans.id_jurusan')->get()
        );
        return view('gedung', $data);
    }

    function tambah(){
        $data = array(
            "jurusan" => jurusans::get()
        );
        return view('gedung_tambah', $data);
    }

    function proses_tambah(Request $request){
        $request->validate([
            'gedung' => "required",
            'kd_ruangan' => "required",
            'id_jurusan' => "required"
        ],[
            "required" => "Field Tidak Boleh Kosong!"
        ]);

        $gedung = new gedungs();
        $gedung->nama_gedung = $request->gedung;
        $gedung->kd_ruangan = $request->kd_ruangan;
        $gedung->id_jurusan = $request->id_jurusan;
        if($gedung->save()){
            return redirect('/gedung')->with('sukses','Berhasil Menambahkan Gedung!');
        }
        return redirect('/gedung')->with('gagal','Gagal Menambahkan Gedung');
    }

    function edit($id){
        $data = array(
            "gedung" => gedungs::where('id_gedung',$id)->first(),
            "jurusan" => jurusans::get()
        );
        return view('gedung_edit', $data);
    }

    function proses_edit(Request $request){
        $request->validate([
            "gedung" => "required",
            'kd_ruangan' => "required",
            'id_jurusan' => "required"
        ],[
            "required" => "Field Tidak Boleh Kosong!"
        ]);

        if(gedungs::where('id_gedung',$request->id)->update(['nama_gedung' => $request->gedung, 'id_jurusan' => $request->id_jurusan, 'kd_ruangan' => $request->kd_ruangan])){
            return redirect()->route('gedung')->with('sukses','Update Berhasil');
        }
        return redirect()->route('gedung')->with('gagal','Update Gagal');
    }

    function hapus($id){
        if(gedungs::where('id_gedung',$id)->delete()){
            return redirect()->route('gedung')->with('sukses','Hapus Berhasil');
        }
        return redirect()->route('gedung')->with('gagal','Hapus Gagal');
    }
}
