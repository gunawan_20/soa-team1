@extends("masterdashboard")

@section('title')
<title> DATA PELANGGAN </title>
@endsection

@section('External_CSS_JS')

@endsection

@section("judul_laman")

@endsection

@section("konten")

<!-- Jangan lupa bungkus dengan "container" -->
<div class="container mx-auto mb-4" style="width: 90%;">
    <h4 class="pt-2">TABEL DATA PELANGGAN</h4>
        <p class="mb-4">Sumber Data : Sistem Informasi Pengembangan Pelanggan</p>
    <div class="card">
        <div class="row">
            <div class="col">
                <div class ="mx-auto pt-5 pb-5" id="container" style="width: 90%;">
                    <div class="container cardBoxHasil">
                        {{-- <h4 class="text-center mb-5">TABEL DATA PELANGGAN</h4> --}}
                        <table class="table">                          
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Data Pengelolaan Pelanggan</th>
                                    <th scope="col">Jumlah</th>
                                </tr>
                            </thead>
                            <tbody class="table-group-divider border-primary">
                                <tr>
                                    <th scope="row">1</th>
                                    <td><p>Pesan WhatsApp Keluar</p></td>
                                    <td>{{ $datapelanggan['jumlah_whatsapp_keluar'] }}</td>
                                </tr>

                                <tr>
                                    <th scope="row">2</th>
                                    <td><p>Pesan Email Keluar</p></td>
                                    <td>{{ $datapelanggan['jumlah_email_keluar'] }}</td>
                                </tr>

                                <tr>
                                    <th scope="row">3</th>
                                    <td><p>Pesanan Diterima</p></td>
                                    <td>{{ $datapelanggan['jumlah_pesanan_diterima'] }}</td>
                                </tr>

                            </tbody>

                        </table>
                    </div>   
                </div>
            </div>
        </div>
    </div>
</div>

@endsection