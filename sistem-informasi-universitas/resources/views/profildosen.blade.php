@extends('master')
@section('konten')
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Data Dosen</title>
  </head>
  <body>
    <h3 class="text-center mb-4 mt-4">Data Dosen</h3>
    <div class="container">
      <div class="row">
        <div class="col-8">
          <a href="/tambahDosen" type="button" class="btn btn-primary mb-4 mt-4">+ Tambah</a>
        </div>
        <div class="col-md-4">
          <form action="/profildosen">
            <div class="input-group mb-4 mt-4">
             <form action="#" method="GET">
              <input type="search" name="search" class="form-control" placeholder="search" aria-label="search" aria-describedby="button-addon2">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="button" id="button-addon2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                      <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                    </svg>
                  </button>
                </div>
            </div>
          </form>
        </div>
        <div class="row">
          {{--@if($message = Session::get('success'))
          <div class="alert alert-success" role="alert">
              Data berhasil ditambahkan!
            </div>
            @endif--}}
<div class="card mt-3" style="width: 98%;">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">NIP Dosen</th>
            <th scope="col">Nama Dosen</th>
            <th scope="col">Email</th>
            <th scope="col">Alamat</th>
            <th scope="col">Jabatan</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($data as $index=>$row)
    
            <tr>
                <th scope="row">{{ $index + $data->firstitem() }}</th>
                <td>{{ $row->id_dosen }}</td>
                <td>{{ $row->nama_dosen }}</td>
                <td>{{ $row->email }} </td>
                <td>{{ $row->alamat }} </td>
                <td>{{ $row->jabatan }}</td>
                <td>
                  <a href="/tampildosen/{{ $row->id_dosen }}" class="btn btn-success">Edit</a>
                  <a href="#" type="button" class="btn btn-danger delete" data-id_dosen="{{ $row->id_dosen }}" data-id_dosen="{{ $row->id_dosen }}">Delete</button>
              </td>
            </tr>
                
            @endforeach
                  
          </tbody>
        </table>
      </div>
  </div>
  <div class="row mt-4">
    <div class="col-8">
      <a href="/dashboard" class="btn btn-dark">Kembali </a>
    </div>
    <div class="col align-self-end">
      {{ $data->links() }}
    </div>
  </div>
</div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
  $('.delete').click(function(){
    var id_dosen=$(this).attr('data-id_dosen');
    swal({
      title: "Apakah Kamu Yakin?",
      text: "Kamu akan menghapus data dosen dengan NIP "+id_dosen+"!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location="/deletedosen/"+id_dosen+"";
        swal("Data berhasil di hapus!", {
          icon: "success",
        });
      } else {
        swal("Data gagal di hapus");
      }
    });
  });
</script>
<script>
  @if (Session::has('success'))
  // Set a success toast, with a title
  toastr.success("{{Session::get('success')}}")
  @endif    
</script>
</html>

    
@endsection