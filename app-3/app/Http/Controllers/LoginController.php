<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    //
    public function loginmahasiswa(){
        return view('loginmahasiswa');
    }

    public function loginmahasiswasimpan(Request $request){
        if (Auth::attempt($request->only('email','password'))) {
            return redirect('/mahasiswa');
        }
        return redirect('/loginmahasiswa');
    }
    public function registermahasiswa(){
        return view('registermahasiswa');
    }

    public function registermahasiswasimpan(Request $request){
        User::create([
            'name' => $request->name,
            'email' =>$request->email,
            'password'=> bcrypt($request->password),
            'remember_token'=>Str::random(68),
        ]);

        return redirect('/loginmahasiswa');
    }

    public function logoutmahasiswa(){
        Auth::logout();
        return redirect('/loginmahasiswa');
    }
    
    //controller login matakuliah
    public function prelogin(){
        return view('prelogin');
    }
    
    public function login(){
        return view('login');
    }

    public function register(){
        return view('register');
    }

    public function lupapass(){
        return view('lupapass');
    }

    public function loginUser(){
        return view('loginUser');
    }
    
    public function registerUser(){
        return view('registerUser');
    }

    public function lupapassUser(){
        return view('lupapassUser');
    }
    
}
