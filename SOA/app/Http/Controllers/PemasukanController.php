<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PemasukanController extends Controller
{
    public function pemasukan(){
        // data transaksi
       $request = Http::get('http://kelompok6.live/api/sales');
       $response = $request -> getBody()->getContents();
       $data = json_decode($response,true);
       
       $terjual = $data['Jumlah_barang_terjual'];
       $transaksi = $data['Jumlah_transaksi_penjualan'];
       $pembelian = $data['Jumlah_transaksi_pembelian'];
       return view('userPemasukan', compact('data'));
    }
}
