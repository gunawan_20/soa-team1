@extends('master')
    
@section('konten')
<h2 class="mb-3 text-center">Edit User</h2>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-8">
<div class="card">
  <div class="card-body">
    <form action="/updatebyid/{{ $data->id }}" method="POST">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Nama</label>
            <input type="text" name="name" class="form-control"  id="name" value ="{{ $data->name }}">
          </div>
        <div class="mb-3">
          <label for="email" class="form-label">Email</label>
          <input type="email" class="form-control" name ="email" id="email" value ="{{ $data->name }}">
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">Password</label>
          <input type="password" class="form-control" name ="password" id="password" value="{{ $data->password }}">
        </div>
        <button type="submit" class="btn btn-primary login">Update</button>
      </form>
      <a href="/user" id="kembali"><button type="submit" class="btn btn-danger d-inline mt-2">Kembali</button></a>
  </div>
</div>
</div>
</div>
</div>
@endsection