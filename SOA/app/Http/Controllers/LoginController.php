<?php

namespace App\Http\Controllers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Models\User;
class LoginController extends Controller
{
    public function login()
    {
            return view('loginbaru');
    }
    public function dashboard()
    {
        return view('dashboard');
    }
   
    public function loginUser(Request $request)
    {
        $data =[
            'email' => $request->input('email'),
            'password'=>$request->input('password')
        ];
        if (Auth::attempt($data)) {
            return redirect('pegawai');
        }else {
            return redirect('/');
        }
    }
    public function logOut(){
        Auth::logout();
        return redirect('/');
    }
    public function create(Request $request)
    {

        $user = new User;
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->password = $request->password;

        

        return "Data Berhasil masuk";

    }
    public function update(Request $request, $id)
    {
        $nama = $request->nama;
        $password = $request->password;

        $user = User::find($id);
        $user->name = $nama;
        $user->password = $password;
        $user->save();

        return "Berhasil Update Data";
    }
}
