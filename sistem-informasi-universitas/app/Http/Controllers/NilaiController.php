<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use App\Models\Matkul;
use App\Models\Nilai;
use Illuminate\Http\Request;

class NilaiController extends Controller
{
    public function dataNilai(Request $request){
        if($request->has('search')){
            $data = Nilai::where('nilai', 'LIKE', '%' . $request->search.'%')->paginate(5);
        }
        else{
            $data = Nilai::with('mahasiswa', 'matkul')->paginate(5);
        }
        return view('nilai', compact('data'));
    }
    
    public function tambahNilai(){
        $datanim = Mahasiswa::all();
        $datamk = Matkul::all();
        return view('tambahNilai', compact([
            'datanim',
            'datamk'
        ]));
    }
    
    public function insertDataNilai(Request $request){
        Nilai::create($request->all());
        return redirect()->route('Data Nilai');
    }
    
    public function tampilNilai($nilai){
        $data = Nilai::find($nilai);
        return view('tampilNilai', compact('data'));
    }
    
    public function editNilai(Request $request, $id){
        $data = Nilai::find($id);
        $data->update($request->all());
        return redirect()->route('Data Nilai');
    }
    
    public function deleteNilai($id){
        $data = Nilai::find($id);
        $data->delete();
        return redirect()->route('Data Nilai');
    }
    
    public function mhsNilai(Request $request){
        if($request->has('search')){
            $data = Nilai::where('nilai', 'LIKE', '%' . $request->search.'%')->paginate(5);
        }
        else{
            $data = Nilai::with('mahasiswa', 'matkul')->paginate(5);
        }
        return view('mhsNilai', compact('data'));
    }
    
}
