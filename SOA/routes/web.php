<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\DataApiController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BarangController;

use App\Http\Controllers\userinventarisController;

use App\Http\Controllers\PelangganController;
use App\Http\Controllers\PemasukanController;

use App\Http\Controllers\PenjualanController;
use App\Http\Controllers\PortalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function (){
    return view('welcome');
});

Route::get('/portal', [PortalController::class, 'portal']);
Route::get('dashboard',function ()
{
    return view('dashboard');
});
Route::get('/logindashboardinternal',[LoginController::class,'login'])->name('login');
Route::post('/login/kirim',[LoginController::class,'loginUser'])->name('loginaksi');

Route::get('/auth/google',[AuthenticateController::class,'redirect']);
Route::get('/auth/callback',[AuthenticateController::class,'callback']);

Route::get('pengembanganpelanggan',[DataApiController::class,'index']);

Route::get('pegawai',[DataApiController::class,'pegawai']);
//Route::get('insertToAPi',[DataApiController::class,'insertToAPi']);

Route::get('barang',[BarangController::class, 'getData'])->name('barang');

Route::get('pemasok',function ()
{
    return view('pemasok');
});

Route::get('pembelian',function ()
{
    return view('pembelian');
});

Route::get('penjualan',[PenjualanController::class, 'penjualan']);

Route::get('/userDashboard',function ()
{
    return view('userDashboard');
});

Route::get('userPemasukan',[PemasukanController::class, 'pemasukan']);

Route::get('/userPengeluaran',function ()
{
    return view('userPengeluaran');
});
Route::get('/userKas',function ()
{
    return view('userKas');
});
Route::get('/userInventaris',[userinventarisController::class, 'Datainventaris'])->name('userInventaris');

Route::get('/dataPegawai',[DataApiController::class,'pegawaiInternal']);
Route::get('/dataPelanggan',function ()
{
    return view('dataPelanggan');
});


Route::get('dataPelanggan',[PelangganController::class, 'pelanggan']);

Route::get('/dataPemasok',function ()
{
    return view('dataPemasok');
});