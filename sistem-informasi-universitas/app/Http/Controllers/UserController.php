<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class UserController extends Controller
{
    //
    public function dataUser (Request $request)
    {
        if ($request->has('search')) {
            $data = User::where('email','LIKE','%'.$request->search.'%')->paginate(5);
        }else{
            $data = User::paginate(5);
        }

        return view('user',compact('data'));

    }
    public function tambahUser()
    {
        return view('tambahUser');
    }

    public function insertDataUser(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' =>$request->email,
            'role' =>$request->role,
            'password'=> bcrypt($request->password),
            'remember_token'=>Str::random(68),
        ]);
        return redirect()->route('Data User');
    }

    public function deleteUser($id)
    {
        $data = User::find($id);
        $data->delete();

        return redirect('user');
    }

    public function updatedatauser($id)
    {
        $data=User::find($id);

        return view('updateUser',compact('data'));
    }

    public function updateUser(Request $request,$id)
    {
        $data = User::find($id);
        $data-> update ([
            $request->name,
            $request->email,
            'password'=> bcrypt($request->password),
            'remember_token'=>Str::random(68),
        ]);

        return redirect('user');
    }


    
}
