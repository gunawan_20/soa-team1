<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <title>ERP Datahouse</title>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

      <!-- Google Font -->
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link rel="preconnect" href="https://fonts.googleapis.com">
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
      <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Viga&display=swap" rel="stylesheet">

      <!-- Styledatahouse.css -->
      <link rel="stylesheet" href="{{ asset('styledatahouse.css') }}">

   </head>
   <body>

        <!-- Nav Bar -->
        <nav class="navbar navbar-expand-lg navbar-light">
          <div class="container">
              <a class="navbar-brand" href="/"> 
                <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="home-img bi-house-door" viewBox="0 0 16 16">
                <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
                </svg>ERP Datahouse</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="/">Landing Page</a>
                  </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ url('/portal') }}">Dashboard Data</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ url('/logindashboardinternal') }}">Data Internal</a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ url('logindashboardinternal') }}" class="btn btn-primary btn-lg btn-menu" type="button" id="myLogin"> LOGIN <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="login-img bi-box-arrow-in-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"/>
                        <path fill-rule="evenodd" d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                        </svg></a>
                    </li>
                </ul>
              </div>
          </div>
        </nav>
<!-- Last Navbar -->

<!-- Jumbotron -->
<div class="jumbotron p-5 mb-4 bg-light rounded-3">
  <div class="container py-5">
    <h5 class="display-4">Selamat datang di,</h5>
    <h1 class="display-5 fw-bold">Ekosistem Data ERP</h1>
    <a href="{{ url('/portal') }}" class="btn btn-primary btn-lg btn-data" type="button"> Telusur Data </a>
  </div>
</div>
<!-- Last Jumbotron -->


<!-- Container-->
<div class="row justify-content-center">
  <div class="col-9 info-panel">
    <div class="row">
{{-- Gambar 1 --}}
      <div class="col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-lightning-charge-fill" viewBox="0 0 16 16" class="float-left">
          <path d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"/>
          <h5> Akses Cepat Tanpa Batas </h5>
          <p> 24 jam melayani kebutuhan anda akan data kami</p>
        </svg>
      </div>
{{-- Gambar 2 --}}
      <div class="col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-check2-all" viewBox="0 0 16 16"  class="float-left">
          <path d="M12.354 4.354a.5.5 0 0 0-.708-.708L5 10.293 1.854 7.146a.5.5 0 1 0-.708.708l3.5 3.5a.5.5 0 0 0 .708 0l7-7zm-4.208 7-.896-.897.707-.707.543.543 6.646-6.647a.5.5 0 0 1 .708.708l-7 7a.5.5 0 0 1-.708 0z"/>
          <path d="m5.354 7.146.896.897-.707.707-.897-.896a.5.5 0 1 1 .708-.708z"/>
          <h5> Data Akurat dan Terkini </h5>
          <p> Data yang relevan dan dapat dipercaya </p>
        </svg>
      </div>
{{-- Gambar 3 --}}
      <div class="col-lg">
        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="bi bi-pie-chart-fill" viewBox="0 0 16 16"  >
          <path d="M15.985 8.5H8.207l-5.5 5.5a8 8 0 0 0 13.277-5.5zM2 13.292A8 8 0 0 1 7.5.015v7.778l-5.5 5.5zM8.5.015V7.5h7.485A8.001 8.001 0 0 0 8.5.015z"/>
          <h5> Tampil Ramah dan Bersahabat </h5>
          <p> Mudah dipahami pengguna </p>
        </svg>
      </div>

    </div>
  </div>
</div>

<!-- akhir Container -->
  
<!-- space -->
<div class="container">
    <h2 class="space">Ekosistem Data ERP </h2>

    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor" class="line bi-dash-lg" viewBox="0 0 16 16">
      <path fill-rule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11A.5.5 0 0 1 2 8Z"/>
    </svg>
    <p class="description">Ekosistem Data ERP adalah Portal terintegrasi untuk informasi perusahaan yang bisa diakses masyarakat umum dan pegawai tiap saat  </p>
</div>

<!-- akhir space -->

<div class="container py-5">
    <!-- Cards -->
  <div class="cardBox d-flex justify-content-center">
    <div class="contentBox">
      <div class="iconBox">
        <img src="images/portal-data.png" alt="space" class="image">
        <h5> Dashboard Data </h5>
        <p> Portal integrasi data umum perusahaan bagi masyarakat</p>
        <a href="{{ url('/portal') }}" class="btn btn-primary btn-lg btn-menu2" type="button"> Telusur Data </a>
      </div>
    </div>

    <div class="contentBox">
      <div class="iconBox">
        <img src="images/data-internal.png" alt="space" class="image">
        <h5> Data Internal </h5>
        <p> Portal integrasi data internal perusahaan bagi staff perusahaan</p>
          <a href="{{ url('logindashboardinternal') }}" class="btn btn-primary btn-lg btn-menu2" type="button"> Telusur Data </a>
      </div>
    </div>
  </div>
</div>


<!-- end space -->
<div class="d-flex flex-column flex-md-row text-center text-md-start justify-content-between py-3 bg-dark">
      <!-- Copyright -->
      <div class="container">
        <div class="text-white">
        Copyright © 2022. All rights reserved.
        </div>
    </div>
</div>
   </body>
</html>