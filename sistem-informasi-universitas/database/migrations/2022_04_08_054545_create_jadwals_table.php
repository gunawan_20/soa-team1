<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwals', function (Blueprint $table) {
            $table->id();
            $table->integer('id_jadwal');
            $table->primary('id_jadwal')->length(6);
            $table->integer('id_dosen');
            $table->string('nama_mk');
            $table->enum('hari',['senin','selasa','rabu','kamis']);
            $table->time('jam_awal');
            $table->time('jam_selesai');
            $table->char('id_gedung');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
    }
}
