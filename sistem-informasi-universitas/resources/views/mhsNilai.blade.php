@extends('master')

    
@section('konten')

<h3>NILAI MAHASISWA</h3>
<div class="input-group mb-3">
  <form action="/mhsNilai" method="get">
    <input type="search" name ="search" class="form-control" placeholder="Cari Berdasarkan Nilai" >
  </form>
</div>
<div class="card mt-3" style="width: 98%;">
    <table class="table">
        <thead>
          <tr>
            <th scope="col">NO</th>
            <th scope="col">NIM</th>
            <th scope="col">MAHASISWA</th>
            <th scope="col">MATAKULIAH</th>
            <th scope="col">NILAI</th>
            <th scope="col">KETERANGAN</th>
          </tr>
        </thead>
        <tbody>
          @php
            $no=1;  
          @endphp
            @foreach ($data as $index => $baris)
    
            <tr>
                <th scope="row">{{ $index+$data->firstItem() }}</th>
                <td>{{ $baris->mahasiswa->NIM }}</td>
                <td>{{ $baris->mahasiswa->nama_mhs }}</td>
                <td>{{ $baris->matkul->nama_matkul }}</td>
                <td>{{ $baris->nilai }}</td>
                <td>{{ $baris->komentar }}</td>
            </tr>
                
            @endforeach
        </tbody>
        {{ $data->links() }}
      </table>
  </div>

    
@endsection